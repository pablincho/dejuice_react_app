const express = require('express') 
const fileUpload = require('express-fileupload');
var cors = require('cors')
const handlers = require('./lib/handlers')
var bodyParser = require("body-parser");

const app = express()

app.use(fileUpload());
app.use(bodyParser.urlencoded({ limit: "20mb", extended: true }));
app.use(bodyParser.json({limit: "20mb"}));
app.use(cors())

const port = process.env.PORT || 4000;

app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", '*'); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/sav_convert', function(req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let savFile = req.files.sav_file;
  handlers.savFileProcess(req, res, savFile)
});

app.post('/upload_excel', function(req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let excelFile = req.files.file;
  handlers.excelFileProcess(req, res, excelFile)
});

app.post('/getTable', function(req, res){
  var bannerVals = req.body.bannervars;
  var stubVals = req.body.stubvars;
  var dataMeta = req.body.dataMeta;
  var dataVars = req.body.dataVars;
  var options = req.body.options;
  handlers.getTableResult(req, res, bannerVals, stubVals, dataMeta, dataVars, options)
})

app.post('/weightsBuilder', function(req, res){
  handlers.weightsBuilder(req, res)
  let weights = req.body.data;
  console.log(weights);
})

app.post('/printTable', function(req, res){
  let tableJSON = req.body.tableJSON;
  handlers.printTable(req, res, tableJSON)
})

app.get('/warmEndpoint', function(req, res){
  res.status(200).send('OK').end();
})

// app.use(handlers.notFound)
// app.use(handlers.serverError)

module.exports = app
