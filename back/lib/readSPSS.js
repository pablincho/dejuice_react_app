function lengthInBytes(type) {
    var totalStringBytes = parseInt(type / 8);
    if (type % 8) {
      totalStringBytes += 1;
    }
    return totalStringBytes;
  }
  
  function intToType(type) {
    if(type === 0)
      return "number"
    if(type === -1)
      return "noneType"
    else
      return "string"
  }
  
  function annotateVariable(type) {
    return {
      _type: intToType(type),
      _lengthInBytes: lengthInBytes(type)
    };
  }
  // --- *** ---- 
  
  // --- index.js content ---
  var formatTypes = {
    0: "Not used",
    1: "A(String)",
    2: "AHEX",
    3: "COMMA",
    4: "DOLLAR",
    5: "F(Numeric)",
    6: "IB",
    7: "PIBHEX",
    8: "P",
    9: "PIB",
    10: "PK",
    11: "RB",
    12: "RBHEX",
    13: "Not used",
    14: "Not used",
    15: "Z",
    16: "N",
    17: "E",
    18: "Not used",
    19: "Not used",
    20: "DATE",
    21: "TIME",
    22: "DATETIME",
    23: "ADATE",
    24: "JDATE",
    25: "DTIME",
    26: "WKDAY",
    27: "MONTH",
    28: "MOYR",
    29: "QYR",
    30: "WKYR",
    31: "PCT",
    32: "DOT",
    33: "CCA",
    34: "CCB",
    35: "CCC",
    36: "CCD",
    37: "CCE",
    38: "EDATE",
    39: "SDATE",
    40: "MTIME",
    41: "YMDHMS"
  };
  
  var actual_variable_count;
  
  var parseBuffer = function(buffer) {
    if (!Buffer.isBuffer(buffer)) {
      throw new Error("argument buffer is not a Buffer object");
    }
    var emptyVariable = 0;
    var offset = 0;
    var vars = {};
    vars.dataRecord = [];
    vars.variablesInfo = [];
    // File header record
    vars.rec_type = buffer.toString("utf8", offset, offset + 4);
    offset += 4;
    vars.prod_name = buffer.toString("utf8", offset, offset + 60);
    offset += 60;
    vars.layout_code = buffer.readUInt32LE(offset);
    offset += 4;
    vars.nominal_case_size = buffer.readUInt32LE(offset);
    offset += 4;
    vars.compressed = buffer.readUInt32LE(offset);
    offset += 4;
    vars.weight_index = buffer.readUInt32LE(offset);
    offset += 4;
    vars.ncases = buffer.readUInt32LE(offset);
    offset += 4;
    vars.bias = buffer.readDoubleLE(offset);
    offset += 8;
    vars.creation_date = buffer.toString("utf8", offset, offset + 9);
    offset += 9;
    vars.creation_time = buffer.toString("utf8", offset, offset + 8);
    offset += 8;
    vars.file_label = buffer.toString("utf8", offset, offset + 64);
    offset += 64;
    vars.padding = buffer.slice(offset, offset + 3);
    offset += 3;
    // End of file header record
    // Variable Record
    vars.variables = [];
    for (var $tmp5 = 0; $tmp5 < vars.nominal_case_size; $tmp5++) {
      var $tmp6 = {};
      $tmp6.rec_type = buffer.readUInt32LE(offset);
      offset += 4;
      $tmp6.type = buffer.readInt32LE(offset);
      offset += 4;
  
      vars.variablesInfo.push(annotateVariable($tmp6.type));
      if($tmp6.type === 0)
        $tmp6.type = "number";
      else if($tmp6.type === -1) {
        emptyVariable += 1;
        $tmp6.type = "noneType";
      }
      else
        $tmp6.type = "string";
  
      let aux_has_label = buffer.readUInt32LE(offset);
      $tmp6.has_label = aux_has_label === 1 ? true : false;
      offset += 4;
      $tmp6.n_missing = buffer.readUInt32LE(offset);
      offset += 4;
      $tmp6.print_format = {};
      $tmp6.print_format.decimal_places = buffer.readUInt8(offset);
      offset += 1;
      $tmp6.print_format.field_width = buffer.readUInt8(offset);
      offset += 1;
      $tmp6.print_format.format_type = buffer.readUInt8(offset);
      offset += 1;
      pfUnused = buffer.readUInt8(offset);
      offset += 1;
      $tmp6.print_format.format_type =
        formatTypes[$tmp6.print_format.format_type];
      $tmp6.write_format = {};
      $tmp6.write_format.decimal_places = buffer.readUInt8(offset);
      offset += 1;
      $tmp6.write_format.field_width = buffer.readUInt8(offset);
      offset += 1;
      $tmp6.write_format.format_type = buffer.readUInt8(offset);
      offset += 1;
      wfUnused = buffer.readUInt8(offset);
      offset += 1;
      $tmp6.write_format.format_type =
        formatTypes[$tmp6.write_format.format_type];
      $tmp6.varname = buffer.toString("utf8", offset, offset + 8);
      offset += 8;
      if ($tmp6.has_label) {
        $tmp6.label_length = buffer.readUInt32LE(offset);
        offset += 4;
        $tmp6.variable_label = buffer.toString(
          "utf8",
          offset,
          offset + $tmp6.label_length
        );
        offset += function() {
          return Math.ceil(this.label_length / 4.0) * 4;
        }.call($tmp6, vars);
      }
      $tmp6.missings = buffer.slice(
        offset,
        offset +
          function() {
            return this.n_missing * 64;
          }.call($tmp6, vars)
      );
      offset += function() {
        return this.n_missing * 64;
      }.call($tmp6, vars);
      vars.variables.push($tmp6);
    }
    // Remove from vars.variables objects whose .type == "noneType"
    for (let i = vars.variables.length - 1; i >= 0; i -= 1) {
      if (vars.variables[i].type === "noneType") {
        vars.variables.splice(i, 1);
      }
    }
    // End of variable record
  
    // Value labels record
    vars.labels = [];
    do {
      var $tmp10 = {};
      $tmp10.rec_type = buffer.readUInt32LE(offset);
      offset += 4;
      $tmp10.label_count = buffer.readUInt32LE(offset);
      offset += 4;
      $tmp10.value_labels = [];
      for (var $tmp11 = 0; $tmp11 < $tmp10.label_count; $tmp11++) {
        var $tmp12 = {};
        $tmp12.value = buffer.readDoubleLE(offset);
        offset += 8;
        $tmp12.label_length = buffer.readUInt8(offset);
        offset += 1;
        $tmp12.label = buffer.toString(
          "utf8",
          offset,
          offset + $tmp12.label_length
        );
        offset += function() {
          var round_label_length,
            tmp_mod = (this.label_length + 1) % 8;
  
          if (tmp_mod !== 0) {
            round_label_length = this.label_length + 8 - tmp_mod;
          } else {
            round_label_length = this.label_length;
          }
          return round_label_length;
        }.call($tmp12, vars);
        $tmp10.value_labels.push($tmp12);
      }
      $tmp10.value_label_variables = {};
      $tmp10.value_label_variables.rec_type_2 = buffer.readUInt32LE(offset);
      offset += 4;
      $tmp10.value_label_variables.var_count = buffer.readUInt32LE(offset);
      offset += 4;
      $tmp10.value_label_variables.vars = [];
      for (
        var $tmp14 = 0;
        $tmp14 < $tmp10.value_label_variables.var_count;
        $tmp14++
      ) {
        var $tmp15 = buffer.readUInt32LE(offset);
        offset += 4;
        $tmp10.value_label_variables.vars.push($tmp15);
      }
      vars.labels.push($tmp10);
      
      
    } while (
      !function(item, buffer) {
        return buffer.readUInt32LE(0) !== 3;
      }.call(this, $tmp10, buffer.slice(offset))
      );
    vars.labels.forEach(element => {
      element.value_label_variables.vars.forEach(subelement => {
        vars.variablesInfo[subelement - 1].value_label_var = element.value_labels
      });
    });
    // End of value labels record
    
    // Remove from variablesInfo objects whose ._type == "noneType"
    for (let i = vars.variablesInfo.length - 1; i >= 0; i -= 1) {
      if (vars.variablesInfo[i]._type === "noneType") {
        vars.variablesInfo.splice(i, 1);
      }
    }
    actual_variable_count = vars.nominal_case_size - emptyVariable;
  
    // Records
    vars.records = [];
    do {
      var $tmp17 = {};
      $tmp17.rec_type = buffer.readUInt32LE(offset);
      offset += 4;
      switch ($tmp17.rec_type) {
        // Document Record
        case 6:
          $tmp17.n_lines = buffer.readUInt32LE(offset);
          offset += 4;
          $tmp17.lines = [];
          for (var $tmp18 = 0; $tmp18 < $tmp17.n_lines; $tmp18++) {
            var $tmp19 = {};
            $tmp19.line_width = buffer.toString("utf8", offset, offset + 80);
            offset += 80;
            $tmp17.lines.push($tmp19);
          }
          break;
        // All records with rec_type = 7 (check subtype)
        case 7:
          $tmp17.subtype = buffer.readUInt32LE(offset);
          offset += 4;
          switch ($tmp17.subtype) {
            // Machine Integer Info Record
            case 3:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.version_major = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.version_minor = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.version_revision = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.machine_code = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.floating_point_rep = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.compression_code = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.endiannes = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.character_code = buffer.readUInt32LE(offset);
              offset += 4;
              break;
            // Machine Floating-Point Info Record
            case 4:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.sysmis = buffer.readDoubleLE(offset);
              offset += 8;
              $tmp17.highest = buffer.readDoubleLE(offset);
              offset += 8;
              $tmp17.lowest = buffer.readDoubleLE(offset);
              offset += 8;
              break;
            // Multiple Response Sets Records
            case 7:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              temp_mrsets = buffer.toString(
                "utf8",
                offset,
                offset + $tmp17.count
              );
              offset += $tmp17.count;
              $tmp17.mrsets = {};
              let myIndex = temp_mrsets.indexOf("=");
              $tmp17.mrsets.name = temp_mrsets.slice(1, myIndex);
              $tmp17.mrsets.set_type = temp_mrsets.slice(
                myIndex + 1,
                temp_mrsets.indexOf(" ", myIndex + 1)
              );
              myIndex = temp_mrsets.indexOf(" ", myIndex + 1);
              if ($tmp17.mrsets.set_type.includes("D")) {
                $tmp17.mrsets.counted_value = temp_mrsets.slice(
                  myIndex + 1,
                  temp_mrsets.indexOf(" ", myIndex + 1)
                );
                myIndex = temp_mrsets.indexOf(" ", myIndex + 1);
                $tmp17.mrsets.label_length = temp_mrsets.slice(
                  myIndex + 1,
                  temp_mrsets.indexOf(" ", myIndex + 1)
                );
                myIndex = temp_mrsets.indexOf(" ", myIndex + 1);
                $tmp17.mrsets.label = temp_mrsets.slice(
                  myIndex + 1,
                  Number(myIndex + 1) + Number($tmp17.mrsets.label_length)
                );
                myIndex =
                  Number(myIndex + 1) + Number($tmp17.mrsets.label_length);
                let varNames = temp_mrsets.slice(
                  myIndex + 1,
                  temp_mrsets.indexOf("\n")
                );
                $tmp17.mrsets.var_names = [];
                $tmp17.mrsets.var_names = varNames.split(" ");
              }
              break;
            // Extra Product Info Record
            case 10:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.info = buffer.toString(
                "utf8",
                offset,
                offset + $tmp17.count
              );
              offset += $tmp17.count;
              break;
            // Variable Display Parameter Record
            case 11:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.varsParams = [];
              for (
                var $tmp22 = 0;
                $tmp22 <
                function() {
                  return actual_variable_count;
                }.call($tmp17, vars);
                $tmp22++
              ) {
                var $tmp23 = {};
                $tmp23.measure = buffer.readUInt32LE(offset);
                offset += 4;
                switch (
                  function() {
                    return $tmp17.count / actual_variable_count;
                  }.call($tmp23, vars)
                ) {
                  case 2:
                    $tmp23.alignment = buffer.readUInt32LE(offset);
                    offset += 4;
                    break;
                  case 3:
                    $tmp23.width = buffer.readUInt32LE(offset);
                    offset += 4;
                    $tmp23.alignment = buffer.readUInt32LE(offset);
                    offset += 4;
                    break;
                  default:
                    throw new Error(
                      "Met undefined tag value " +
                        function(item) {
                          // return $tmp17.count/item.nominal_case_size
                          return $tmp17.count / 55;
                        }.call($tmp23, vars) +
                        " at choice"
                    );
                }
                $tmp17.varsParams.push($tmp23);
              }
              break;
            // Long Variable Names Record
            case 13:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              aux_var_name_pairs = buffer.toString(
                "utf8",
                offset,
                offset + $tmp17.count
              );
              offset += $tmp17.count;
              $tmp17.var_name_pairs = aux_var_name_pairs.split("\t");
              for (let i = 0; i < $tmp17.var_name_pairs.length; i++) {
                $tmp17.var_name_pairs[i] = $tmp17.var_name_pairs[i].slice(
                  $tmp17.var_name_pairs[i].indexOf("=") + 1,
                  $tmp17.var_name_pairs[i].length
                );
              }
              var longVarNames = $tmp17.var_name_pairs;
              break;
            // Very Long String Record
            case 14:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.string_lengths = buffer.toString(
                "utf8",
                offset,
                offset + $tmp17.count
              );
              offset += $tmp17.count;
              break;
            // Extended Number of Cases Record
            case 16:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.unknown = buffer.readFloatLE(offset);
              offset += 8;
              $tmp17.ncases64 = buffer.readFloatLE(offset);
              offset += 8;
              break;
            // Data File and Variable Attributes Records
            case 17:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.info = buffer.toString(
                "utf8",
                offset,
                offset + $tmp17.count
              );
              offset += $tmp17.count;
              break;
            // Variable Attributes Records
            case 18:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.attributes = buffer.toString(
                "utf8",
                offset,
                offset + $tmp17.count
              );
              offset += $tmp17.count;
              break;
            // Character Encoding Record
            case 20:
              $tmp17.size = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.count = buffer.readUInt32LE(offset);
              offset += 4;
              $tmp17.encoding = buffer.toString(
                "utf8",
                offset,
                offset + $tmp17.count
              );
              offset += $tmp17.count;
              break;
            default:
              throw new Error(
                "Met undefined tag value " + $tmp17.subtype + " at choice"
              );
          }
          break;
        default:
          throw new Error(
            "Met undefined tag value " + $tmp17.rec_type + " at choice"
          );
      }
      vars.records.push($tmp17);
    } while (
      !function(item, buffer) {
        return buffer.readUInt32LE(0) === 999;
      }.call(this, $tmp17, buffer.slice(offset))
    );
    // -- Replace variables.varname by .var_name_pairs (from Long Variable Names Record) --
    for (let i = 0; i < vars.variables.length; i++) {
      vars.variables[i].varname = longVarNames[i];
    }
    // Data termination record
    vars.DTR_rec_type = buffer.readUInt32LE(offset);
    offset += 4;
    // Filler
    vars.filler = buffer.readUInt32LE(offset);
    offset += 4;
    // Data record
    for (var $tmp25 = 0; offset < buffer.length; $tmp25++) {
      var $tmp26 = {};
      $tmp26.Data = [];
      for (var $tmp27 = 0; $tmp27 < 8; $tmp27++) {
        var $tmp28 = buffer.readUInt8(offset);
        offset += 1;
        $tmp26.Data.push($tmp28);
      }
      vars.dataRecord.push($tmp26);
    }
    return vars;
  };
  
  var parse = function(buffer) {
    let vars = parseBuffer(buffer);
    var varsInfo = vars.variablesInfo;
    var varTypeInfoIndex = 0;
    var totalCount = 0;
    var splIndex = 0;
    var indices = [];
  
    var isAStringArray = [];
    var orgDataArray = [];
    var outputArray = [];
    var currentLength = 0;
  
    function stringTypeIndex(value) {
      if (value < actual_variable_count) {
        return value;
      } else {
        return stringTypeIndex(value - actual_variable_count);
      }
    }
  
    function popVariable() {
      varTypeInfoIndex += 1;
      if (varTypeInfoIndex >= actual_variable_count) {
        varTypeInfoIndex = 0;
      }
    }
  
    function checkFullLength(varLength) {
      currentLength += 1; // if 253 data is or not string
      if (currentLength == varLength) {
        currentLength = 0;
        popVariable();
      }
    }
  
    for (var i = 0; i < vars.dataRecord.length; i++) {
      let item;
      if (i == 0 || i == totalCount) {
        item = vars.dataRecord[i].Data;
        var idx = item.indexOf(253);
        while (idx != -1) {
          //Searches 253 along the 8 elements array
          indices.push(idx + orgDataArray.length); //Stores the indexes in 'indices' array
          idx = item.indexOf(253, idx + 1);
        }
        var count = item.filter(function(x) {
          //'count' holds the number of 253 appearances in item
          return x === 253;
        }).length;
        totalCount += count + 1;
        for (var j = 0; j < 8; j++) {
          let blockItem = item[j];
          let varInfo = varsInfo[varTypeInfoIndex];
          let varLength = varInfo._lengthInBytes;
          if (1 <= blockItem && blockItem <= 251) {
            // if 1 <= element <= 251 is actual data
            item[j] -= vars.bias; // and it must be substracted from bias content
            orgDataArray.push(item[j]); // after that saved to output array
            popVariable();
          } else if (blockItem == 253) {
            if (varInfo._type == "string") {
              isAStringArray.push(true);
              checkFullLength(varLength);
            } else {
              isAStringArray.push(false);
              popVariable();
            }
          } else if (blockItem == 254) {
            orgDataArray.push("");
            checkFullLength(varLength);
          } else if (blockItem == 255) {
            item[j] = null;
            orgDataArray.push(item[j]);
            popVariable();
          }
        }
      } else {
        // Regrouping uint8 into a buffer to read strings
        let tempBuffer = Buffer.from(vars.dataRecord[i].Data);
        let splItem = indices[splIndex];
        if (!isAStringArray[0]) {
          item = tempBuffer.readDoubleLE(0);
        } else if (isAStringArray[0]) {
          item = tempBuffer.toString("utf8", 0, 8);
        }
        orgDataArray.splice(splItem, 0, item);
        isAStringArray.shift();
        splIndex += 1;
      }
      vars.dataRecord[i] = item;
    }
    concatStringsArr = orgDataArray.map((element, index, array) => {
      if (typeof element == "string") {
        varTypeInfoIndex = stringTypeIndex(index);
        for (let i = 1; i < varsInfo[varTypeInfoIndex]._lengthInBytes; i++) {
          element += array[index + i];
        }
        array.splice(index + 1, varsInfo[varTypeInfoIndex]._lengthInBytes - 1);
        element = element.trim();
      }
      return element;
    });
    for (let j = concatStringsArr.length - 1; j >= 0; j -= 1) {
      if (concatStringsArr[j] === undefined) {
        concatStringsArr.splice(j, 1);
      }
    }
    // -- Stores the dataRecord organized in an output matrix --
    var chunks = function(array, size) {
      var results = [];
      var current = 0;
      const parts = array.length / size;
      for (let index = 0; index < parts; index++) {
        const part = array.slice(current, current + size);
        current += size;
        results.push(part);
      }
      return results;
    };
  
    outputArray = chunks(concatStringsArr, actual_variable_count);
  
    let var_indices = {};
  
    vars.variables.forEach((variable, index) => {
      var_indices[variable.varname] = index;
    })
  
    vars.records.forEach(record_type =>{
      if (record_type.rec_type === 7 && record_type.subtype === 7) {
        const mrset_vars = record_type.mrsets.var_names;
        upper_case_vars = mrset_vars.map(mrset_var => mrset_var.toUpperCase());
        mrset_indexes = upper_case_vars.map(mrset_var => var_indices[mrset_var]);
        let first_mrset_index = var_indices[upper_case_vars[0]];
        outputArray = outputArray.map(row => {
          popped_vars = []
          for(let j = mrset_indexes.length - 1; j >= 0; j -= 1){
            popped_vars = popped_vars.concat(row.splice(mrset_indexes[j], 1));
          }
          row.splice(first_mrset_index, 0, popped_vars.reverse());
          return row;
        });
      }
    });
  
    vars.varsInfo =  vars.variablesInfo;
    vars.outputArray = outputArray;
    return vars;
  };
  // --- *** ----
  
  // --- quantipy-formatter.js content ---
  
  const formatter = function(buffer) {
    let preVars = parse(buffer)
    const vars = {
      info: {
        text: "Converted from SAV file .",
        from_source: {
          dejuice_reader: "sav"
        }
      },
      lib: {
        "default text": "main",
        values: {}
      },
      masks: {},
      sets: {
        "data file": {
          text: {
            "en-GB": "Variable order in source file"
          },
          items: []
        }
      },
      type: "pandas.DataFrame",
      columns: {},
      data: preVars.outputArray
    };
    var mrSets = {};
  
    // -- sets --
  const dataFile = vars.sets["data file"];

  // -- Copy all variables in items --
  preVars.variables.forEach(variable => {
    dataFile.items.push("columns@" + variable.varname);
  });
  // -- look for mrsets and replace mrsets variables in items by mrsets names --
  preVars.records.forEach(record_type => {
    if (record_type.rec_type === 7 && record_type.subtype === 7) {
      mrSets = record_type.mrsets;
      const mrset_vars = record_type.mrsets.var_names;
      let lowerCaseVars = mrset_vars.map(mrset_var => mrset_var.toLowerCase());
      let lowerCaseItems = dataFile.items.map(item => item.toLowerCase());

      let firstMrsetIdx = lowerCaseItems.indexOf(
        "columns@" + lowerCaseVars[0]
      );
      
      let mrsetIdx = firstMrsetIdx;
      dataFile.items = dataFile.items.filter((element, idx) => {
        if(idx === mrsetIdx && mrsetIdx < (firstMrsetIdx + mrset_vars.length)){
          mrsetIdx += 1;
          return false
        }
        else{
          return true
        }
      });

      dataFile.items.splice(
        firstMrsetIdx,
        0,
        "columns@" + record_type.mrsets.name
      );
    }
  });
var isEmpty = function(obj) {
  if(Object.keys(obj).length === 0 && obj.constructor === Object) return true
  else return false
};
  if (!isEmpty(mrSets)) {
    // -- builds the mrset variable object (qd8:{} in this example)
    var outputMrSets = {};
    outputMrSets.name = mrSets.name;
    outputMrSets.parent = {};
    outputMrSets.properties = {};
    outputMrSets.type = "delimited set";
    outputMrSets.text = {};
    outputMrSets.text["en-GB"] = mrSets.label;
    outputMrSets.values = [];
    // tooks variable_label from each mrset variable
    mrSets.var_names.forEach((mrSetVar, index) => {
      preVars.variables.forEach(bufferVar => {
        if (bufferVar.varname.toLowerCase() === mrSetVar.toLowerCase()) {
          outputMrSets.values.push({
            text: {
              ["en-GB"]: bufferVar.variable_label
            },
            value: index + 1
          });
        }
      });
    });
  } else {
    var mrSets;
    mrSets.var_names = [];
  }

  // all variables except mrset variables
  preVars.variables.forEach((element, index) => {
    if (!mrSets.var_names.includes(element.varname.toLowerCase())) {
      const colItem = {};
      vars.columns[element.varname] = colItem;
      colItem.name = element.varname;
      colItem.text = {};
      colItem.text["en-GB"] =
        element.variable_label == undefined ? "" : element.variable_label;
      colItem.parent = {};
      colItem.properties = {};
      colItem.type = preVars.varsInfo[index]._type;
      if (colItem.type === "number") {
        if (preVars.varsInfo[index].value_label_var) {
          colItem.type = "single";
        } else {
          if (element.print_format.decimal_places === 0) {
            colItem.type = "int";
          } else {
            colItem.type = "float";
          }
        }
      }
      if (preVars.varsInfo[index].value_label_var) {
        colItem.values = [];
        preVars.varsInfo[index].value_label_var.forEach(subvalue => {
          var dict = {};
          dict.text = {};
          dict.text["en-GB"] = subvalue.label;
          dict.value = subvalue.value;
          colItem.values.push(dict);
        });
      }
    } else {
    }
  });
  if (outputMrSets) {
    vars.columns[outputMrSets.name] = outputMrSets;
  }
  return vars;
  };
  
  exports.formatter = formatter;