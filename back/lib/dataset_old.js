const read_spss_file = require('./readSPSS')
const d3 = require("d3");
const fs = require('fs');
const ExcelJS = require('exceljs')

Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}

const  newMetaVariable = function(varDetails){
    var options = {
      percentages: false,
      counts: true,
      wVar: '',
      total: true, 
      base: true, 
      wBase: true,
      eBase: true
    };
    var varsTable = $('#edit_datable_1');
    var newVar = {};
    newVar.text = {};
    newVar.values = [];
    newVar.name = name.toUpperCase();
    newVar.text['en-GB'] = description;
    variableList.meta.columns[newVar.name] = newVar;
    variableList.meta.sets['data file'].items.push('columns@' + newVar.name);
    $('#variables_list').remove();
    var htmlContent = template(variableList.meta);
    $(".variable_container").html(htmlContent);
    showModal(variableList.meta, varsTable);
    makeTable();
    $('#run_table').off("click");
    runTable(variableList.meta, variableList.data, options);
    if(name.toLowerCase()==="total"){
      variableList.data.forEach((dataRow, dataRowIndx) => {
        dataRow["TOTAL"] = "1";
      });
      addTotal(variableList.meta.columns[name.toUpperCase()])
    }else{
      addCategory(variableList.meta.columns[name], variableList.meta.columns.QD1.values[0])
    }
    
  
  };

const multiVariableStub = function(multiVar,meta,data){
    // return meta.columns[multiVar].type;
    let newMultiVars = [];
    meta.columns[multiVar].values.forEach(function (multiCategory, multiCategoryIndex) {
        var newVar = {};
        newVar.text = {};
        newVar.values = [];
        newVar.name = meta.columns[multiVar].name.toUpperCase()+"_"+multiCategory.value.toString();
        newVar.text['en-GB'] = multiCategory.text['en-GB'];
        meta.columns[newVar.name] = newVar;
        meta.sets['data file'].items.push('columns@' + newVar.name);
        newVar.values.push({
            text: {'en-GB': multiCategory.text['en-GB']},
            value: multiCategory.value
        })
        data.forEach((dataRow, dataRowIndx) => {
            if(Array.isArray(dataRow[multiVar])){
            if(dataRow[multiVar][multiCategory.value-1]===1){
                dataRow[newVar.name] = multiCategory.value;
            }
            }
        });
        newMultiVars.push(newVar.name);
    });
    return [newMultiVars, meta, data];
}

const buildMultiVarStubTable = function(newMultiVars, bannerVar, weightVar, newMeta, newData){
    let selectedWeightedVar = weightVar;
    let meta = newMeta;
    let data = newData;
    let bannerVars=bannerVar;
    let stubVarRow = [];
    newMultiVars.forEach(function (stubVar, stubVarIndex) {
        bannerVars.forEach(function (bannerVar, bannerVarIndex) {
            data.forEach(function(d) {
                d[stubVar] = +d[stubVar];
                d[bannerVar] = +d[bannerVar];
            });
            var cleaned = data.filter(function(d) { 
                return d[stubVar] !=="" && d[bannerVar] !=="";
            });
            let weight = false;
            var stubByBanner = d3.nest()   
                .key(function(d) { return d[stubVar]; }).sortKeys(d3.ascending)
                .key(function(d) { return d[bannerVar]; }).sortKeys(d3.ascending)
                .entries(cleaned)
                .map(function(d){
                    var values = d.values.map(function(dd){        
                        if(weight){
                            var count =  dd.values.length;
                            return { category_value: parseInt(dd.key), counts:count}
                        }else{
                            var count =  dd.values.length;
                            var weightedcount =  dd.values.map(function(ddd){
                                return(ddd[selectedWeightedVar]);
                            })
                            // var weightedcountsquared =  dd.values.map(function(ddd){
                            //     return(Math.pow(ddd[selectedWeightedVar],2));
                            // })
                            const reducer = (accumulator, currentValue) => parseFloat(accumulator) + parseFloat(currentValue);
                            let arrSum = weightedcount.reduce(reducer);
                            // let arrSumSquare = weightedcountsquared.reduce(reducer);
                            if(typeof(arrSum)!='string'){
                                var weightedcount = parseFloat(arrSum);
                                // var weightedcountsquared = parseFloat(arrSumSquare.toFixed(1));
                            }else{
                                var weightedcount = parseFloat(parseFloat(arrSum));
                                // var weightedcountsquared = parseFloat(arrSumSquare.toFixed(1));
                            }
                            return { category_value: parseInt(dd.key), counts:count, weightedcounts:weightedcount}     
                        }   
                    });
                    values.sort((a, b) => a.category_value - b.category_value);
                    var base = values.reduce((prev,next) => prev + next.counts,0);
                    if(weight){
                        return { category_value: parseInt(d.key), rowbase: base,  values:values}
                    }else{
                        var weighted_base = values.reduce((prev,next) => prev + next.weightedcounts,0);
                        return { category_value: parseInt(d.key), weighted_rowbase: weighted_base, rowbase: base,  values:values}
                    }               
                });   
            var merged = [].concat.apply([], stubByBanner);
            var decoratedTable = decorateTable(merged, meta.columns[stubVar], meta.columns[bannerVar]); 
            decoratedTable.sort((a, b) => a.category_value - b.category_value);
            function compare( a, b ) {
                if ( a.category_value < b.category_value ){
                    return -1;
                }
                if ( a.category_value > b.category_value ){
                    return 1;
                }
                return 0;
            }
            decoratedTable[1]['values']=decoratedTable[1]['values'].sort(compare);
            stubVarRow.push(decoratedTable[1]);    
        });    
    });
    return(stubVarRow);
}

const mergeMultiRows = function(decoratedTable, multiTable){
    for (var i=2; i<decoratedTable.length; i++) {
        decoratedTable[i]=multiTable[i-2];
    }
    return(decoratedTable);
}

const arrayToJSONObject = function(column_headers,data_records){
    //header
    var keys = column_headers;
    //records
    var newArr = data_records;
  
    var formatted = [],
    data = newArr,
    cols = keys,
    l = cols.length;
    for (var i=0; i<data.length; i++) {
            var d = data[i],
                    o = {};
            for (var j=0; j<l; j++)
                    o[cols[j]] = d[j];
            formatted.push(o);
    }
    return formatted;
}


const decorateTable = function(table, stubVarMeta, bannerVarMeta){
    let rawTable = table;
    let tableStubMeta = stubVarMeta;
    let tableBannerMeta = bannerVarMeta
    let tableHeader  = {};
    tableHeader['row_type'] = 'header';
    tableHeader['banner_header'] = bannerVarMeta.text['en-GB'];
    tableHeader['stub_header'] = stubVarMeta.text['en-GB'];
    tableHeader["values"] = [];
    tableStubMeta.values.forEach(function (stubDetail, stubDetailIndex) {       
        let stubObj = rawTable.find((o, i) => {
            if (o.category_value === stubDetail.value) {
                rawTable[i]["category_text"] = stubDetail.text['en-GB'];
                return true; // stop searching
            }
        });
        if (typeof stubObj === 'undefined'){
            let newEmptyStubCategory = {};
            newEmptyStubCategory["category_text"] = stubDetail.text['en-GB'];
            newEmptyStubCategory["category_value"] = stubDetail.value;
            newEmptyStubCategory["rowbase"] = 0;
            newEmptyStubCategory["weighted_rowbase"] = 0;
            newEmptyStubCategory["values"] = [];
            rawTable.push(newEmptyStubCategory);
        }
        tableBannerMeta.values.forEach(function (bannerDetail, bannerDetailIndex) {
            if(stubDetailIndex===0){
                tableHeader["values"][bannerDetailIndex] = bannerDetail.text['en-GB'];
            }
            if(rawTable[stubDetailIndex].values.length>0){
                let bannerObj = rawTable[stubDetailIndex].values.find((bo, bi) => {
                    if (bo.category_value === bannerDetail.value) {
                        rawTable[stubDetailIndex].values[bi]["category_text"] = bannerDetail.text['en-GB'];
                        return true; // stop searching
                    }
                });
                if (typeof bannerObj === 'undefined'){
                    let newEmptyBannerCategory = {};
                    newEmptyBannerCategory["category_text"] = bannerDetail.text['en-GB'];
                    newEmptyBannerCategory["category_value"] = bannerDetail.value;
                    newEmptyBannerCategory["counts"] = 0;
                    newEmptyBannerCategory["weightedcounts"] = 0;
                    // newEmptyBannerCategory["weightedcountsquared"] = 0;
                    rawTable[stubDetailIndex].values.push(newEmptyBannerCategory);

                }
            } else{
                let newEmptyBannerCategory = {};
                newEmptyBannerCategory["category_text"] = bannerDetail.text['en-GB'];
                newEmptyBannerCategory["category_value"] = bannerDetail.value;
                newEmptyBannerCategory["counts"] = 0;
                newEmptyBannerCategory["weightedcounts"] = 0;
                // newEmptyBannerCategory["weightedcountsquared"] = 0;
                rawTable[stubDetailIndex].values.push(newEmptyBannerCategory);
            }

        })
    });
    rawTable.unshift(tableHeader);
    return rawTable;
}


const addTableBaseRow = function(table){
    let rawTable = table;
    let rawTableBaseTotal = 0;
    let tableBaseRow  = {};
    tableBaseRow['row_type'] = "base row";
    tableBaseRow['values'] = [];
    rawTable.forEach(function (tableRowDetail, tableRowDetailIndex) {       
        if(!('row_type' in tableRowDetail)){
            tableRowDetail.values.sort((a, b) => a.category_value - b.category_value);  
            rawTableBaseTotal+=tableRowDetail['rowbase'];
            tableRowDetail.values.forEach(function (tableCellDetail, tableCellDetailIndex) {
                if(tableRowDetailIndex!=1){
                    tableBaseRow['values'][tableCellDetailIndex]['total_counts'] += tableCellDetail['counts'];
                }else{
                    let newEmptyBaseCategory = {};
                    newEmptyBaseCategory['category_text'] = tableCellDetail['category_text'];
                    newEmptyBaseCategory['category_value'] = tableCellDetail['category_value'];
                    newEmptyBaseCategory['total_counts'] = tableCellDetail['counts'];
                    tableBaseRow['values'].push(newEmptyBaseCategory);
                }
            })
        }
    })
    tableBaseRow['rowbase_total']=rawTableBaseTotal;
    rawTable.splice(1,0,tableBaseRow);
    return rawTable;
}


const addMultiTableBaseRow = function(bannerVar, table, data, meta){
    let rawTable = table;
    let tabledata = data;
    data.forEach(function(d) {
        d[bannerVar] = +d[bannerVar];
    });
    var cleaned = tabledata.filter(function(d) { 
        return d[bannerVar]  !=="";
    });
    var categoriesCount = d3.nest()
        .key(function(d) { return d[bannerVar]; }).sortKeys(d3.ascending)
        .rollup(function(v) { return v.length; })
        .entries(cleaned);
    let tableBaseRow  = {};
    tableBaseRow['row_type'] = "base row";
    tableBaseRow['rowbase_total']=0;
    tableBaseRow['values'] = [];
    meta.columns[bannerVar].values.forEach(function (categoryDetail, categoryDetailIndex){
        let detail = categoriesCount.find(detail => detail.key === categoryDetail.value.toString());
        if(detail){
            tableBaseRow['rowbase_total']+=detail.value;
            let newEmptyBaseCategory = {};
            newEmptyBaseCategory['category_text'] = categoryDetail.text['en-GB'];
            newEmptyBaseCategory['category_value'] = categoryDetail.value;
            newEmptyBaseCategory['total_counts'] = detail.value;
            tableBaseRow['values'].push(newEmptyBaseCategory);
        }else{
            let newEmptyBaseCategory = {};
            newEmptyBaseCategory['category_text'] = categoryDetail.text['en-GB'];
            newEmptyBaseCategory['category_value'] = categoryDetail.value;
            newEmptyBaseCategory['total_counts'] = 0;
            tableBaseRow['values'].push(newEmptyBaseCategory);        
        }
    })
    // rawTable.splice(1,0,tableBaseRow);
    rawTable[1]=tableBaseRow;
    return rawTable;
}

const addWeightedTableBaseRow = function(table){
    let rawTable = table;
    let rawTableBaseTotal = 0;
    let tableBaseRow  = {};
    tableBaseRow['row_type'] = "weighted base row";
    tableBaseRow['rowbase_total']=0
    tableBaseRow['values'] = [];
    rawTable.forEach(function (tableRowDetail, tableRowDetailIndex) {            
        if(!('row_type' in tableRowDetail)){
            tableRowDetail.values.sort((a, b) => a.category_value - b.category_value);  
            rawTableBaseTotal+=tableRowDetail['weighted_rowbase'];
            tableRowDetail.values.forEach(function (tableCellDetail, tableCellDetailIndex) {
                let detail = tableBaseRow['values'].find(detail => detail.category_text === tableCellDetail['category_text']);
                if(typeof detail === 'undefined'){
                    // detail['values']['total_counts'] = tableCellDetail['weightedcounts']
                    let newEmptyBaseCategory = {};
                    newEmptyBaseCategory['category_text'] = tableCellDetail['category_text'];
                    newEmptyBaseCategory['category_value'] = tableCellDetail['category_value'];
                    newEmptyBaseCategory['total_counts'] = tableCellDetail['weightedcounts']
                    tableBaseRow['values'].push(newEmptyBaseCategory);
                }else{
                    detail['total_counts'] += tableCellDetail['weightedcounts']

                }                
            })
        }
    })
    tableBaseRow['rowbase_total']=rawTableBaseTotal;
    rawTable.splice(1,0,tableBaseRow);
    return rawTable;
}

const addWeightedMultiTableBaseRow = function(bannerVar, table, weight, data, meta){
    let rawTable = table;
    let tabledata = data;
    data.forEach(function(d) {
        d[bannerVar] = +d[bannerVar];
    });
    var cleaned = tabledata.filter(function(d) { 
        return d[bannerVar] !=="";
    });
    var categoriesCount = d3.nest()
        .key(function(d) { return d[bannerVar]; }).sortKeys(d3.ascending)
        .rollup(function(v) { return {
            weightedcount: d3.sum(v, function(d) { return d[weight]; }),
        }})
        .entries(cleaned);
    let tableBaseRow  = {};
    tableBaseRow['row_type'] = "weighted base row";
    tableBaseRow['rowbase_total']=0;
    tableBaseRow['values'] = [];
    meta.columns[bannerVar].values.forEach(function (categoryDetail, categoryDetailIndex){
        let detail = categoriesCount.find(detail => detail.key === categoryDetail.value.toString());
        if(detail){
            tableBaseRow['rowbase_total']+=detail.value.weightedcount;
            let newEmptyBaseCategory = {};
            newEmptyBaseCategory['category_text'] = categoryDetail.text['en-GB'];
            newEmptyBaseCategory['category_value'] = categoryDetail.value;
            newEmptyBaseCategory['total_counts'] = detail.value.weightedcount;
            tableBaseRow['values'].push(newEmptyBaseCategory);
        }else{
            let newEmptyBaseCategory = {};
            newEmptyBaseCategory['category_text'] = categoryDetail.text['en-GB'];
            newEmptyBaseCategory['category_value'] = categoryDetail.value;
            newEmptyBaseCategory['total_counts'] = 0;
            tableBaseRow['values'].push(newEmptyBaseCategory);        
        }
    })
    rawTable.splice(1,0,tableBaseRow);
    return rawTable;
}

const addPercentages = function(table, weight){
    let rawTable = table;
    let weightRow;
    let cellItem = "";
    if(weight){                
        weightRow = rawTable.find(base_row_type => base_row_type.row_type === "weighted base row");
        cellItem = 'weightedcounts';
    }else{
        weightRow = rawTable.find(base_row_type => base_row_type.row_type === "base row");
        cellItem = 'counts';
    }
    rawTable.forEach(function (tableRowDetail, tableRowDetailIndex) {      
        if(!('row_type' in tableRowDetail)){
            tableRowDetail.values.forEach(function (tableCellDetail, tableCellDetailIndex) {
                if(isFinite((tableCellDetail[cellItem]/weightRow.values[tableCellDetailIndex]['total_counts'])*100 )){
                    if(tableCellDetailIndex===0){
                    }
                    let cellPercentage = (tableCellDetail[cellItem]/weightRow.values[tableCellDetailIndex]['total_counts'].toFixed(2)*100 );
                    tableCellDetail['percentages'] = cellPercentage;
                }else{
                    tableCellDetail['percentages'] = 0;
                }

            })
        }
    })
    return rawTable;
}

const columnNames = function(columns){
    sortedColumnName = columns.map(function (i) {
        var splitname = i.split("@")
        var columnLabel = splitname[1];
        return columnLabel;
    })
    return sortedColumnName;
}
exports.DataSet =  class DataSet{
    constructor(name,meta,data){
        this.name = name;
        this.data = data;
        this.meta = meta;
    }

    static read_spss(savfile){
        let file = savfile;
        let parsed = read_spss_file.formatter(file.data);
        let savData = parsed.data;
        let savMeta = parsed;
        delete parsed['data'];
        let dataHeader = columnNames(parsed.sets['data file'].items)
        let savDataWithHeaders = arrayToJSONObject(dataHeader, savData);
        // savData.unshift(dataHeader);

        return new this('aSavFile',savMeta,savDataWithHeaders)
    }

    static crossTab(stubVars, bannerVars, dataMeta, dataVars, options){
        let selectedWeightedVar = options.wVar;
        let allTableJSON = {};
        let meta = dataMeta;
        allTableJSON['bannerVars'] = bannerVars;
        allTableJSON['stubVars'] = stubVars;
        allTableJSON['options'] = options;
        let data = dataVars;
        allTableJSON['Tables'] = []
        allTableJSON['tablesByRow'] = [];
        stubVars.forEach(function (stubVar, stubVarIndex) {
            allTableJSON['Tables'][stubVar] = [];
            let stubVarRow = [];
            bannerVars.forEach(function (bannerVar, bannerVarIndex) {
                let multiTable;
                if(dataMeta.columns[stubVar].type === "delimited set"){
                    let [newMultiVars, newMeta, newData] = multiVariableStub(stubVar,dataMeta,dataVars);
                    let selectedBannerVar = [bannerVar];
                    multiTable = buildMultiVarStubTable(newMultiVars, selectedBannerVar, selectedWeightedVar, newMeta, newData);
                };
                data.forEach(function(d) {
                    d[stubVar] = +d[stubVar];
                    d[bannerVar] = +d[bannerVar];
                });
                var cleaned = data.filter(function(d) { 
                    return d[stubVar] !=="" && d[bannerVar] !=="";
                });
                let weight = false;
                var stubByBanner = d3.nest()   
                    .key(function(d) { return d[stubVar]; }).sortKeys(d3.ascending)
                    .key(function(d) { return d[bannerVar]; }).sortKeys(d3.ascending)
                    .entries(cleaned)
                    .map(function(d){
                        var values = d.values.map(function(dd){       
                            if(weight){
                                var count =  dd.values.length;
                                return { category_value: parseInt(dd.key), counts:count}
                            }else{
                                var count =  dd.values.length;
                                var weightedcount =  dd.values.map(function(ddd){
                                    return(ddd[selectedWeightedVar]);
                                })
                                // var weightedcountsquared =  dd.values.map(function(ddd){
                                //     return(Math.pow(ddd[selectedWeightedVar],2));
                                // })
                                const reducer = (accumulator, currentValue) => parseFloat(accumulator) + parseFloat(currentValue);
                                let arrSum = weightedcount.reduce(reducer);
                                // let arrSumSquare = weightedcountsquared.reduce(reducer);
                                if(typeof(arrSum)!='string'){
                                    var weightedcount = parseFloat(arrSum);
                                    // var weightedcountsquared = parseFloat(arrSumSquare.toFixed(1));
                                }else{
                                    var weightedcount = parseFloat(parseFloat(arrSum));
                                    // var weightedcountsquared = parseFloat(arrSumSquare.toFixed(1));
                                } 
                                return { category_value: parseInt(dd.key), counts:count, weightedcounts:weightedcount}     
                            }   
                        });
                        values.sort((a, b) => a.category_value - b.category_value);
                        var base = values.reduce((prev,next) => prev + next.counts,0);
                        if(weight){
                            return { category_value: parseInt(d.key), rowbase: base,  values:values}
                        }else{
                            var weighted_base = values.reduce((prev,next) => prev + next.weightedcounts,0);
                            return { category_value: parseInt(d.key), weighted_rowbase: weighted_base, rowbase: base,  values:values}
                        }               
                    });
            var merged = [].concat.apply([], stubByBanner);
            var decoratedTable = decorateTable(merged, meta.columns[stubVar], meta.columns[bannerVar]);
            
            if(dataMeta.columns[stubVar].type === "delimited set"){
                decoratedTable = mergeMultiRows(decoratedTable, multiTable);
            }
            if(options.base===true){
                if(dataMeta.columns[stubVar].type === "delimited set"){
                    decoratedTable = addMultiTableBaseRow(bannerVar,decoratedTable,data, meta);
                }else{
                    decoratedTable = addTableBaseRow(decoratedTable);
                }
            }
            if(options.wBase===true && options.wVar!=""){
                if(dataMeta.columns[stubVar].type === "delimited set"){
                    decoratedTable = addWeightedMultiTableBaseRow(bannerVar,decoratedTable, selectedWeightedVar, data, meta);
                }else{
                    decoratedTable = addWeightedTableBaseRow(decoratedTable);
                }
            }
            if(options.percentages===true){
                decoratedTable = addPercentages(decoratedTable, selectedWeightedVar);
            }
            decoratedTable.sort((a, b) => a.category_value - b.category_value);
            allTableJSON['Tables'].push(decoratedTable);
            stubVarRow.push(decoratedTable);       

            });
        let flattenedStub  = [].concat.apply([], stubVarRow);
        allTableJSON['tablesByRow'].push(stubVarRow);
        // fs.writeFileSync('tableoutput.json', JSON.stringify(stubVarRow));      

        });
        return(allTableJSON);     
      }
    static async read_tabspec(excelfile, surveyMeta, surveyData){
    // static read_tabspec(excelfile){
        console.log(surveyData);
        let columnData = JSON.parse(surveyMeta);
        let file = excelfile.data;
        let returnedData = {};
        const workbook = new ExcelJS.Workbook();
        await workbook.xlsx.load(file)
        workbook.eachSheet((worksheet, sheetID)=>{
            if(workbook.getWorksheet(sheetID).name==="Variable Definitions"){
                let newVarMeta = [];
                let newDataCalcs = [];
                let newVarCounter = -1;
                let categoryValueCounter=0;
                workbook.getWorksheet(sheetID).eachRow((row, rowIndex)=>{
                    if(rowIndex>1){
                        if(row.values.filter(x => x).length === row.values.length-1 && rowIndex!=1){
                            let variableValues = [];
                            row.values.forEach((newVarVal, newVarValIndex)=>{
                                variableValues.push(newVarVal)
                            })
                            let newVar = {};
                            let newVarCalcs = {};
                            newVar.name = variableValues[0];
                            newVarCalcs.name = variableValues[0];
                            newVar.text = {};
                            newVar.parent = {};
                            newVar.properties = {};
                            newVar.type = 'single';
                            newVar.values = [];
                            newVarCalcs.values = [];
                            categoryValueCounter=1;
                            newVar.text['en-GB'] = variableValues[1];
                            let newVarCategory = {};
                            newVarCategory.value = categoryValueCounter;
                            newVarCategory.text={};
                            newVarCategory.text['en-GB'] = variableValues[2];
                            newVarCalcs.values.push(variableValues[3]);
                            newVar.values.push(newVarCategory);
                            newVarMeta.push(newVar);
                            newDataCalcs.push(newVarCalcs);
                            categoryValueCounter+=1;
                            if(newVarCounter===-1){
                                newVarCounter=0;                               
                            }else{
                                newVarCounter+=1;
                            }
                        }else{
                            let variableValues = [];
                            row.values.forEach((newVarVal, newVarValIndex)=>{
                                variableValues.push(newVarVal)
                            })
                            let newVarCategory = {};
                            newVarCategory.value = categoryValueCounter;
                            newVarCategory.text={};
                            newVarCategory.text['en-GB'] = variableValues[0];
                            newVarMeta[newVarCounter].values.push(newVarCategory);
                            newDataCalcs[newVarCounter].values.push(variableValues[1]);
                            categoryValueCounter+=1;
                        }
                    }else{
                        let spec_headers = [];
                        row.values.forEach((headerVal, headerValIndex)=>{
                            spec_headers.push(headerVal)
                        })
                        if(spec_headers=['Variable Name','Variable Label','Category Labels','Category Definitions']){
                            console.log('header', row.values, rowIndex);
                        }else{
                            console.log('your document headers are not correctly formatted')
                        }    
                    }
                    returnedData.meta=newVarMeta;
                    returnedData.data=newDataCalcs; 
                })

            }
        })        
        return JSON.stringify(returnedData);
    }
}