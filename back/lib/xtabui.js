const fs = require('fs');
const ExcelJS = require('exceljs');

const sortVarMeta = function(columns,columnMeta){
    sortedVarMeta = columns.map(function (i) {
        var spltname = i.split("@")
        var columnLabel = spltname[1];
        return columnMeta[columnLabel];
    })
    return sortedVarMeta;
}

const columnNames = function(columns){
    sortedColumnName = columns.map(function (i) {
        var splitname = i.split("@")
        var columnLabel = splitname[1];
        return columnLabel;
    })
    return sortedColumnName;
}

const tableJSONtoExcel = function(tableJSON){
    let tableData = tableJSON;
    let workbook = new ExcelJS.Workbook();
    let worksheet = workbook.addWorksheet('Table1');
    let row_start_position=4;
    let table_row_position=0;
    let col_start_position=0;
    let categories_start = false;
    const cell_format = { name: 'open sans', size: 10, bold: true };
    const cell_format_non_bold = { name: 'open sans', size: 10, color:{ argb: 'FF000000' }};
    const stub_format = { name: 'open sans', size: 10, bold: true };
    const weighted_format = { name: 'open sans', size: 10, color:{ argb: 'FF800000' }};
    const unweighted_format = { name: 'open sans', size: 10, color:{ argb: 'FF969696'}};
    const cell_border = {top: {style:'thin'},left: {style:'thin'},bottom: {style:'thin'},right: {style:'thin'}};
    tableData.forEach((rowOfTables, rowOfTablesIndex) => {
        for(i=0; i<rowOfTables[0].length; i++){
            if(i===0){
            categories_start = false
            }
            for(x=0; x<rowOfTables.length; x++){
            if(rowOfTablesIndex===0 && rowOfTables[x][i].row_type){
                switch(rowOfTables[x][i].row_type) {
                case "header":
                    console.log(rowOfTables[x][i].row_type)
                    worksheet.mergeCells(row_start_position,(col_start_position+2),row_start_position,(col_start_position+2)+rowOfTables[x][i].values.length-1);
                    worksheet.getCell(row_start_position,(col_start_position+2),row_start_position,(col_start_position+2)+rowOfTables[x][i].values.length-1).value = rowOfTables[x][i].banner_header;
                    worksheet.getCell(row_start_position,(col_start_position+2),row_start_position,(col_start_position+2)+rowOfTables[x][i].values.length-1).font = cell_format;
                    worksheet.getCell(row_start_position,(col_start_position+2),row_start_position,(col_start_position+2)+rowOfTables[x][i].values.length-1).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
                    worksheet.getCell(row_start_position,(col_start_position+2),row_start_position,(col_start_position+2)+rowOfTables[x][i].values.length-1).border = cell_border;
                    rowOfTables[x][i].values.forEach((tableBannerElement,tableBannerElementIndex) =>{
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).value = tableBannerElement;
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).font = cell_format;
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).border = cell_border;
                    })
                    col_start_position+=rowOfTables[x][i].values.length;
                    break;
                case "weighted base row":
                    if(x===0){
                    row_start_position+=1;                
                    col_start_position=0;
                    worksheet.getCell(row_start_position+1,col_start_position+1).value = "Weighted Sample";
                    worksheet.getCell(row_start_position+1,col_start_position+1).font = weighted_format;
                    worksheet.getCell(row_start_position+1,col_start_position+1).alignment = {vertical: 'middle', horizontal: 'center' };
                    }
                    rowOfTables[x][i].values.forEach((tableBannerElement,tableBannerElementIndex) =>{
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).value = Math.round(tableBannerElement.total_counts);
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).font = cell_format_non_bold;
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).border = cell_border;
                    })
                    col_start_position+=rowOfTables[x][i].values.length;
                    break;
                case "base row":
                    if(x===0){
                    row_start_position+=1;                
                    col_start_position=0;
                    worksheet.getCell(row_start_position+1,col_start_position+1).value = "Unweighted Sample";
                    worksheet.getCell(row_start_position+1,col_start_position+1).font = unweighted_format;
                    worksheet.getCell(row_start_position+1,col_start_position+1).alignment = {vertical: 'middle', horizontal: 'center' };
                    }
                    rowOfTables[x][i].values.forEach((tableBannerElement,tableBannerElementIndex) =>{
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).value = tableBannerElement.total_counts;
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).font = cell_format_non_bold;
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
                    worksheet.getCell(row_start_position+1,col_start_position+2+tableBannerElementIndex).border = cell_border;
                    })
                    col_start_position+=rowOfTables[x][i].values.length;
                    break;
                }
            }else if(rowOfTablesIndex===0 && !rowOfTables[x][i].row_type){
                if (categories_start===false){
                row_start_position+=2;
                var getRowInsert = worksheet.getRow(++(row_start_position));
                getRowInsert.getCell('A').value = rowOfTables[0][0].stub_header;
                getRowInsert.getCell('A').font = cell_format;
                getRowInsert.getCell('A').alignment = { wrapText: true };
                getRowInsert.commit();
                categories_start=true;                
                }
                if(x===0){
                row_start_position+=1;                
                col_start_position=0;
                worksheet.getCell(row_start_position,col_start_position+1).value = rowOfTables[x][i].category_text;
                worksheet.getCell(row_start_position,col_start_position+1).font = cell_format_non_bold;
                worksheet.getCell(row_start_position,col_start_position+1).alignment = {horizontal: 'right', wrapText: true };
                }
                rowOfTables[x][i].values.forEach((tableElement,tableElementIndex, categoryRows) =>{
                    if(tableElement.hasOwnProperty("percentages")){
                        worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).value = Math.round(tableElement.percentages);
                    }else{
                        worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).value = tableElement.counts;
                    }
                    worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).font = cell_format_non_bold;
                    worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
                    if(tableElementIndex===categoryRows.length-1){
                        worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).border ={right: {style:'thin'}};
                    }          
                })
                table_row_position=rowOfTables[x].length-3
                col_start_position+=rowOfTables[x][i].values.length;
            }else if(rowOfTablesIndex!==0 && !rowOfTables[x][i].row_type){
                if (categories_start===false){
                row_start_position+=1;
                var getRowInsert = worksheet.getRow(++(row_start_position));
                getRowInsert.getCell('A').value = rowOfTables[0][0].stub_header;
                getRowInsert.getCell('A').alignment = { wrapText: true };
                getRowInsert.getCell('A').font = stub_format;
                getRowInsert.commit();
                categories_start=true;                
                }
                if(x===0){
                row_start_position+=1;                
                col_start_position=0;
                worksheet.getCell(row_start_position,col_start_position+1).value = rowOfTables[x][i].category_text;
                worksheet.getCell(row_start_position,col_start_position+1).font = cell_format_non_bold;
                worksheet.getCell(row_start_position,col_start_position+1).alignment = {horizontal: 'right' };
                }
                rowOfTables[x][i].values.forEach((tableElement,tableElementIndex, categoryRows) =>{
                    if(tableElement.hasOwnProperty("percentages")){
                        worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).value = Math.round(tableElement.percentages);
                    }else{
                        worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).value = tableElement.counts;
                    }
                    worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).font = cell_format_non_bold;
                    worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
                    if(tableElementIndex===categoryRows.length-1){
                        worksheet.getCell(row_start_position,col_start_position+2+tableElementIndex).border ={right: {style:'thin'}};
                    } 
                })
                col_start_position+=rowOfTables[x][i].values.length;
            }
            }
        }
    });
    worksheet.getColumn(1).width=40;
    return workbook;
}


exports.sortVarMeta = sortVarMeta;
exports.columnNames = columnNames;
exports.tableJSONtoExcel = tableJSONtoExcel;
