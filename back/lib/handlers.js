const fortune = require('./fortune')
const dataset = require('./dataset')
const xtabui = require('./xtabui')
const d3 = require("d3");
const ExcelJS = require('exceljs');


// exports.home = (req, res) => res.render('home')

// exports.about = (req, res) =>
//   res.render('about', { fortune: fortune.getFortune() })

// exports.notFound = (req, res) => res.render('404')

// Express recognizes the error handler by way of its four
// argumetns, so we have to disable ESLint's no-unused-vars rule
/* eslint-disable no-unused-vars */
// exports.serverError = (err, req, res, next) => res.render('500')
/* eslint-enable no-unused-vars */
                                                                                                                                                             
exports.savFileProcess = (req, res, savfile) => { 
  var myDataSet = dataset.DataSet.read_spss(savfile);
  res.status(200).send(myDataSet).end();
}

exports.excelFileProcess = async(req, res, excelfile) => { 
  //Here goes the proccessing of the excel file
  var worksheetname = await dataset.DataSet.read_tabspec(excelfile);
  res.status(200).send(worksheetname).end();
}

exports.getTableResult = (req, res, bannerVals, stubVals, dataMeta, dataVars, options) => { 
  let tableResult = dataset.DataSet.crossTab(stubVals, bannerVals, dataMeta, dataVars, options);
  res.status(200).json(tableResult).end();
}
exports.getDatasmoothieData= (req, res) => { 
  res.status(200).send().end();
}

exports.weightsBuilder = (req, res) => { 
  res.status(200).send().end();
}

exports.printTable = (req, res, tableJSON) => {
  let getExcel = xtabui.tableJSONtoExcel(tableJSON);
  res.attachment("test.xlsx")
  res.set({
    'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'Content-Disposition': `attachment; filename="export.xlsx"`,
  });
  getExcel.xlsx.write(res).then(function() {
    res.end()
    });;

  // res.status(200).send().end();
}