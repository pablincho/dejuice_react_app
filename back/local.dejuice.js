const app = require('./dejuice')
// const port = process.env.PORT || 4000;
const port = 4000;

app.listen(port, () => {
  console.log( `Express started on http://localhost:${port}` +
    '; press Ctrl-C to terminate.' )
})
