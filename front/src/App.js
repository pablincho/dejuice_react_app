import React, { Component } from 'react'
import Router from './shared/Router'
import { DndProvider } from 'react-dnd'
import Backend from 'react-dnd-html5-backend'
import './vendor/styles/bootstrap.scss'
import './vendor/styles/appwork.scss'
import './vendor/styles/theme-corporate.scss'
import './vendor/styles/colors.scss'
import './vendor/styles/uikit.scss'
import './App.scss'

const styles = {
  fontFamily: 'Lexend Deca, sans-serif',
  fontSize: '14px',
  lineHeight: 1.42857143,
  color: '#333333'
}
class App extends Component {
  render() {
    return <DndProvider backend={Backend}>
      <Router/>
    </DndProvider>
  }
}

export default App
