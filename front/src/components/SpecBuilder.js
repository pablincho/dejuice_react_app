import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import { DropTarget } from 'react-dnd';
import  ItemTypes  from '../shared/layouts/ItemTypes';
import '../css/custom.css';
class SpecBuilder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      calcWeightAllowed: false
    }
  }
  render() {
    const { connectDropTarget, isOver } = this.props;
    return (<div>
        <div className="row line-below">
          <div className="col my-3 d-flex align-items-baseline justify-content-start">
            <h1 className="h2 m-0 align-self-center">Spec Builder</h1>
          </div>  
          <div className="col my-3 d-flex align-items-baseline justify-content-center">

          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-6 col-lg-12">
            <Card className="mt-2 w-100 h-100">
                <Card.Title className="m-3">Tab Specification<Button className="m-3" variant="outline-secondary">Import Tab Spec here</Button></Card.Title>
                <Card.Body className="m-3">
                  <Table className="table table-bordered table-responsive-md table-striped text-center">
                    <thead>
                      <tr>
                        <th className="text-center">Variable Name</th>
                        <th className="text-center">Variable Label</th>
                        <th className="text-center">Category Label</th>
                        <th className="text-center">Category Defintion</th>
                        <th className="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td className="pt-3-half" contenteditable="true"></td>
                        <td className="pt-3-half" contenteditable="true"></td>
                        <td className="pt-3-half" contenteditable="true"></td>
                        {connectDropTarget(<td className="pt-3-half" style={isOver ? {backgroundColor: 'lightgray'}  : null} contenteditable="true"></td>)}
                        <td>
                          <span className="category-add">
                            <button type="button" className="btn btn-success btn-rounded btn-sm my-0">Add Category</button>
                          </span>
                          <span className="table-remove">
                            <button type="button" className="btn btn-danger btn-rounded btn-sm my-0">Remove</button>
                          </span>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                </Card.Body>
            </Card>
          </div>
        </div>
      </div>)
  }
}

export default DropTarget(
  ItemTypes.VariableList,
  {
    drop(props, monitor, component) {
      return { name: 'WeightingList' }
    }
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
  }),
)(SpecBuilder)
