import React, { Component } from 'react';
import MainScreen from '../shared/layouts/MainScreen';
class TableBuilder extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <MainScreen stubVars={this.props.stubVars} bannerVars={this.props.bannerVars}
      deleteDraggedVar = {this.props.deleteDraggedVar} enableShowTable={this.props.enableShowTable} showTable={this.props.showTable}
      meta={this.props.meta} data={this.props.data} modalInfo={this.props.showModal} options={this.props.options}
      currentTableObj={this.props.currentTableObj}/>
    )
  }
}

export default TableBuilder
