import React, { Component } from 'react';
import { Nav, Button, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import OwlCarousel from 'react-owl-carousel';
import dejuice_logo from '../img/dejuice_orange_segment_sml_grn.png';
import dejuice_laptop from '../img/dejuice-macbook-gold.png';
import dejuice_background from '../img/dejuice_background_lft.png'
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import '../css/HomeStyles.css';
class Home extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    console.log(this.props)
    const Options = {
      items: 4,
      dots: false,
      loop:true,
      margin:10,
      autoplay:true,
      autoplayTimeout:1000,
      autoplayHoverPause:true
    };
    return (
      <div>
        <Navbar style={{backgroundColor: "white"}} expand="lg" fixed="top">
          <Navbar.Brand href="#home"><img src={dejuice_logo} width="160" height="40" alt="dejuice_logo" /></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="#main">Product</Nav.Link>
              <Nav.Link href="#features">Features</Nav.Link>
              <Nav.Link href="#review-section">Reviews</Nav.Link>
              <Nav.Link href="#pricing">Pricing</Nav.Link>
              <Nav.Link href="#contact">Contact</Nav.Link>
              <Link to="/main/table_builder" className="nav-link"><span><i className="fal fa-table"></i> Table Builder</span></Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="main app form" style={{marginTop: '73.5px'}} id="main">
          <div className="hero-section py-3">
            <div className="row">
              <div className="col-md-6 p-4 d-flex justify-content-center align-items-center">
              <img className="img-responsive w-75" src={dejuice_background} alt="dejuice orange" />
              </div>
              <div className="col-md-6 pr-4 d-flex justify-content-center align-items-center">
              <div className="hero-content py-0">
                <h1 className="font-green mb-3">dejuice</h1>
                <h2><i>/dɪˈdjuːs/</i></h2>
                <h3 className="sub_sub_header">fruitful data analysis </h3>               
                <h4><i>verb</i></h4>
                <ol className="definition">
                  <li>squeeze every last bit of insight from your data.</li>
                  <li>arrive at (a fact or conclusion) by reasoning; draw as a logical conclusion.</li>
                </ol>
              </div>
            </div>
            </div>
          </div>
        </div>
      <div className="client-section p-3 p x-4">
        <OwlCarousel className="clients owl-carousel owl-theme mt-3" {...Options}>
          <div className="item">
            <i className="far fa-weight fa-2x"/><h6>Weighting</h6>
          </div>
          <div className="item">
            <i className="far fa-square-root-alt fa-2x"/><h6>Sig' Test</h6>
          </div>
          <div className="item">
            <i className="far fa-file-excel fa-2x"/><h6>Excel Export</h6>
          </div>
          <div className="item">
            <i className="far fa-edit fa-2x"/><h6>Variable Edits</h6>
          </div>
          <div className="item">
            <i className="fal fa-table fa-2x"/><h6>Tabulation</h6>
          </div>
        </OwlCarousel>
      </div>
      <div className="row p-2 py-5 split-features">
        <div className="col-md-5 p-0">
          <div className="split-content py-0 pl-4">
            <h1 className="wow fadeInUp font-green">The easiest way to build and export market research deliverables</h1>
            <p className="wow fadeInUp">
              dejuice, is deisgned to meet the needs of individuals and companies that need to create attractive and meaningful market research data tables but who want to avoid the time and cost implications of using an outsourced data processing organisation. 
            </p>
          </div>
        </div>
        <div className="col-md-7 d-flex align-items-center">
          <img className="img-responsive wow fadeIn w-100" src={dejuice_laptop} alt="dejuice running on macbook" />
        </div>
      </div>
    </div>
    )
  }
}

export default Home
