import React, { Component } from 'react';
import { Button, Card, Form } from 'react-bootstrap';
import datasmothieOrange from '../img/datasmoothie_orange_api.png';
import '../css/custom.css';
import { DropTarget } from 'react-dnd'
import  ItemTypes  from '../shared/layouts/ItemTypes'
class WeightBuilder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      calcWeightAllowed: false
    }
  }
  render() {
    let calcWeightAllowed = false;
    if(!!this.props.weightVarsList.length){
      calcWeightAllowed = this.props.weightVarsList.reduce((acc, current) => {
        const currentProp = current.calcWeightAllowed;
        return acc && currentProp
      }, true);
    }
    const { connectDropTarget, isOver } = this.props;
    const cardsArr = this.props.weightVarsList.map((weightVar, widx) => {
      return <Card.Body key={widx}>
      <div className="d-flex justify-content-between">
      <Card.Text className="font-weight-bold">
        { weightVar.name }
      </Card.Text>
      <span className="fal fa-trash-alt" style={{cursor: 'pointer'}}
      onClick={()=> {this.props.deleteDraggedWeightVar(weightVar.name)}}></span>
      </div>
      <div className="mb-3">
        <span className="small mb-2">{ weightVar.text !== undefined ? weightVar.text['en-GB'] : null }</span>
      </div>
      {weightVar.values.map((value, idx) => {
        return <div key={idx} className="row my-2">
          <div className="col d-flex align-items-center">
            {value.text !== undefined ? value.text['en-GB'] : null}
          </div>
          <div className="col">
            <input type="number" min="0" max="100" step="0.1" className="form-control" onChange={(e) => this.props.sumWeights(widx, e, idx)}></input>
          </div>
        </div>
      })}
      <div className="row my-2">
        <div className="col d-flex align-items-center"></div>
        <div className="col">
          <input type="number" placeholder="0.0" className="form-control"
          style={{color: weightVar.weightTotal === 100 ? '#4E5155' : '#d9534f', borderColor: weightVar.weightTotal === 100 ? 'rgba(24, 28, 33, 0.1)' : '#d9534f'}} 
          value={weightVar.weightTotal} readOnly></input>
          <small className={weightVar.weightTotal === 100 ? 'text-muted' : 'text-danger'}>The category values must total 100</small>
        </div>
      </div>
    </Card.Body>
    })
    return (
      connectDropTarget(<div>
        <div className="row line-below">
          <div className="col my-3 d-flex align-items-baseline justify-content-start">
            <h1 className="h2 m-0 align-self-center">Weight Builder</h1>
          </div>  
          <div className="col my-3 d-flex align-items-baseline justify-content-center">
            <img src={datasmothieOrange} className="img-fluid align-self-center" height="40"/>
          </div>
          <div className="col my-3 d-flex align-items-baseline justify-content-center">
            <button disabled={!calcWeightAllowed} id="run_table" 
            className="btn btn-outline-secondary align-self-center"
            onClick={this.props.calculateWeight}>
            <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29" viewBox="0 0 23 23" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-play-circle"><circle cx="12" cy="12" r="7"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg>
              Calculate Weights
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-6 col-lg-6">
            <Card className="mt-2 w-100 h-100" style={isOver ? {backgroundColor: 'lightgray'} : null}>
              <Card.Title className="m-3">Weighting Matrix Variables</Card.Title>
              <small className="card-sub-heading">Select variables to add weighting factors</small>
              { cardsArr }
              <Button className="m-3" variant="outline-secondary">Drop single response variable here</Button>
            </Card>
          </div>
          <div className="col-sm-12 col-md-6 offset-lg-1 col-lg-3">
            <Card className="mt-2 w-100 h-100" style={isOver ? {backgroundColor: 'lightgray'} : null}>
              <Card.Title className="m-3">Unique Identifier Variable</Card.Title>
              <small className="card-sub-heading">You must select a numeric variable with unique values</small>
              <div className="card-select">
                <Form.Control as="select" onChange={(e) => this.props.addNumericVar(e)} value={this.props.numericVarInitialValue}>
                  {!!this.props.numericVars ? this.props.numericVars.map((element, idx) => {
                    return <option key={idx}>{element.name}</option>
                  }) : null}
                </Form.Control>
              </div>
            </Card>
          </div>
        </div>
      </div>)
    )
  }
}

export default DropTarget(
  ItemTypes.VariableList,
  {
    drop(props, monitor, component) {
      return { name: 'WeightingList' }
    }
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
  }),
)(WeightBuilder)
