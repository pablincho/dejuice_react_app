import React, { Component } from 'react'
import styles from '../../css/custom.css'
import axios from 'axios'
import { cloneDeep } from 'lodash';

class MainTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableResult: {}
    }
    this.multiVariableStub = this.multiVariableStub.bind(this);
  }

  multiVariableStub = (multiVar,meta,data) => {
    let newMultiVars = [];
    meta.columns[multiVar].values.forEach(function (multiCategory, multiCategoryIndex) {
        var newVar = {};
        newVar.text = {};
        newVar.values = [];
        newVar.name = meta.columns[multiVar].name.toUpperCase()+"_"+multiCategory.value.toString();
        newVar.text['en-GB'] = multiCategory.text['en-GB'];
        meta.columns[newVar.name] = newVar;
        meta.sets['data file'].items.push('columns@' + newVar.name);
        newVar.values.push({
            text: {'en-GB': multiCategory.text['en-GB']},
            value: multiCategory.value
        })
        data.forEach((dataRow, dataRowIndx) => {
          if(Array.isArray(dataRow[multiVar])){
            if(dataRow[multiVar][multiCategory.value - 1]===1){
              console.log(dataRow[multiVar]);
              dataRow[newVar.name] = multiCategory.value;
            }
          }
        });
        newMultiVars.push(newVar.name);
    });
    return [newMultiVars, meta, data];
  }

  drawTable = () =>{
    const stubVars = this.props.stubVars
    const bannerVars = this.props.bannerVars
    const weightVar = this.props.options.wVar;
    let minifiedMeta = {
      columns: {},
      sets: {}
    };
    minifiedMeta.sets['data file'] = {
      items: []
    }
    stubVars.forEach(element => {
      minifiedMeta.columns[element] = this.props.meta.columns[element];

    })
    bannerVars.forEach(element => {
      minifiedMeta.columns[element] = this.props.meta.columns[element];
    })

    if(weightVar!==""){
      minifiedMeta.columns[weightVar] = this.props.meta.columns[weightVar];
    }

    const minifiedData = this.props.data.map(element => {
      let obj = {}
      stubVars.forEach(subelement => {
        obj[subelement] = element[subelement];
      })
      bannerVars.forEach(subelement => {
        obj[subelement] = element[subelement];
      })
      if(weightVar!==""){
        obj[weightVar] = element[weightVar];
      }
      return obj
    });
      var tableParameters = {
        'stubvars': stubVars, 
        'bannervars': bannerVars,
        'dataMeta' : minifiedMeta,
        'dataVars' : minifiedData,
        'options' : this.props.options
      };
      axios({
        // baseURL: "http://localhost:4000",
        baseURL: "https://mn99wezl2h.execute-api.us-east-1.amazonaws.com/production",
        url: "/getTable",
        method: "POST",
        headers: { 'content-type': 'application/json' },
        data: JSON.stringify(tableParameters)
      }).then(res => 
        {
          console.log(res.data);
          const tableResult = res.data;

          this.setState({tableResult})
      }, err => {console.log(err)});

      axios({
        baseURL: "https://dejuiceback.herokuapp.com",
        // baseURL: "https://mn99wezl2h.execute-api.us-east-1.amazonaws.com/production",
        url: "/warmEndpoint",
        method: "GET",
      }).then(res => 
        {

      }, err => {console.log(err)});
  }

  printTable(){
    let data = this.state.tableResult.tablesByRow;
    axios({
      // baseURL: "https://mn99wezl2h.execute-api.us-east-1.amazonaws.com/production",
      baseURL: "https://dejuiceback.herokuapp.com",
      url: "/printTable",
      method: "POST",
      // data: data,
      headers: { 'content-type': 'application/json' },
      data: {
        tableJSON: data,
      },
      responseType: "blob"

    }).then(res => {
      this.handlePrintTable(res);
    }, err => {console.log(err)}); 
  }

  handlePrintTable = (res) => {
    console.log(res)
    const blob = new Blob([res.data]);
    const link = document.createElement("a");

    link.href = URL.createObjectURL(blob);
    link.download = "table_export.xlsx";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
  
  componentDidMount(prevProps){
      this.drawTable()
  }

  componentDidUpdate(prevProps){
    if (this.props.stubVars !== prevProps.stubVars || this.props.bannerVars !== prevProps.bannerVars 
      || this.props.meta !== prevProps.meta || this.props.data !== prevProps.data) {
        this.drawTable();
    }  
  }

  render() {
    const options = this.props.options
    const TablesByBanner = this.state.tableResult.tablesByRow;
    if(typeof TablesByBanner !== 'undefined'){
      let tableHeader=[];
      let tableBody=[]
      let stubValuesRows = [];    
      TablesByBanner.forEach((eachRowOfTables,eachRowOfTablesIndex) =>{
        let bannerHeaderRow = [];
        let unweightedBaseRow = [];
        let weightedBaseRow = [];
        let bannerCategoryRow = [];
        let bannerPercentageSignRow = [];
        let stubHeaderRow =[];
        let stubValuesRow = [];       
        eachRowOfTables.forEach((eachTableInRow,eachTableInRowIndex) =>{
          if(eachRowOfTablesIndex===0){         
            let firstRowOfValues=0
            let stubHeader ="";
            eachTableInRow.forEach((rowOfTable,rowOfTableIndex) =>{
              if(rowOfTable!==null){                    
                if(rowOfTable.hasOwnProperty('row_type')){
                  if(rowOfTable.row_type==='header'){
                    if(eachTableInRowIndex===0){
                      stubHeader=rowOfTable.stub_header;
                      bannerHeaderRow.push(<th key={'pad0'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}></th>);
                    }
                    
                    bannerHeaderRow.push(<th key={'header'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}  className='table_header border' colSpan={rowOfTable.values.length}>{rowOfTable.banner_header}</th>);
                    if(eachTableInRowIndex===0){
                      bannerCategoryRow.push(<th key={'pad1'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}></th>);
                      bannerPercentageSignRow.push(<th key={'padst0w'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}></th>);
                    }
                    rowOfTable.values.forEach((bannerCategoryHeader,bannerCategoryHeaderIndex )=>{
                      bannerCategoryRow.push(<th key={'bannercategorycell'+bannerCategoryHeaderIndex.toString()+rowOfTableIndex.toString()+eachTableInRowIndex.toString()}  className='border'>{bannerCategoryHeader}</th>)
                      bannerPercentageSignRow.push(<th key={'bannerpercentagesign'+bannerCategoryHeaderIndex.toString()+rowOfTableIndex.toString()+eachTableInRowIndex.toString()}  className='border'>%</th>)
                    })
                  }else if(rowOfTable.row_type==='weighted base row'){
                    if(eachTableInRowIndex===0){
                      weightedBaseRow.push(<th key={'weightedcategorycelltitle'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()} className='weighted-base'>Weighted Sample</th>);
                    }
                    rowOfTable.values.forEach((weightedBaseValue,weightedBaseValueIndex )=>{
                      if(weightedBaseValue.total_counts!=null){
                        weightedBaseRow.push(<th key={'unweightedcategorycell'+weightedBaseValueIndex.toString()+rowOfTableIndex.toString()+eachTableInRowIndex.toString()}  className='border'>{weightedBaseValue.total_counts.toFixed(0)}</th>)
                      }else{
                        weightedBaseRow.push(<th key={'unweightedcategorycell'+weightedBaseValueIndex.toString()+rowOfTableIndex.toString()+eachTableInRowIndex.toString()}  className='border'>{weightedBaseValue.total_counts}</th>)
                      }
                    })

                  }else if(rowOfTable.row_type==='base row'){
                    if(eachTableInRowIndex===0){
                      unweightedBaseRow.push(<th key={'unweightedcategorycelltitle'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()} className='base'>Unweighted Sample</th>);
                    }
                    rowOfTable.values.forEach((unWeightedBaseValue,unWeightedBaseValueIndex )=>{
                      unweightedBaseRow.push(<th key={'unweightedcategorycell'+unWeightedBaseValueIndex.toString()+rowOfTableIndex.toString()+eachTableInRowIndex.toString()}  className='border'>{unWeightedBaseValue.total_counts}</th>)
                    })
                  }
                }else{
                  if(firstRowOfValues===0){
                    stubHeaderRow.push(<th key={'stubheader'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}  scope='row'>{stubHeader}</th>);
                    firstRowOfValues+=1;
                  }
                  
                  if(eachTableInRowIndex===0){
                    stubValuesRow[rowOfTableIndex] = [];
                    stubValuesRow[rowOfTableIndex].push(<th key={'stubcategoryheader'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}  className="category-header">{rowOfTable.category_text}</th>);
                  }
                  rowOfTable.values.forEach((categoryCellValue,categoryCellValueIndex,categoryCellArray)=>{
                    let cellContents;
                    if(options.counts && !options.percentages){
                      if(categoryCellValue.counts){
                        cellContents=categoryCellValue.counts.toFixed(0);
                      }
                    }else if(options.percentages && !options.counts){
                      if(categoryCellValue.percentages){
                        cellContents=categoryCellValue.percentages.toFixed(0);
                      }
                    }else if(options.percentages && options.counts){
                      if(categoryCellValue.percentages){
                        cellContents=categoryCellValue.percentages.toFixed(0)+"%\r\n"+categoryCellValue.counts;
                      }
                    }
                    if(categoryCellArray.length-1===categoryCellValueIndex){
                      stubValuesRow[rowOfTableIndex].push(<td key={'stubcategorycell'+eachTableInRowIndex.toString()+categoryCellValueIndex.toString()+rowOfTableIndex.toString()} className='last-category-cell'>{cellContents}</td>);
                    }else{
                      stubValuesRow[rowOfTableIndex].push(<td key={'stubcategorycell'+eachTableInRowIndex.toString()+categoryCellValueIndex.toString()+rowOfTableIndex.toString()} className='category-cell'>{cellContents}</td>);
                    }
                  })           
                }
              }
            })
            //End of iterating each row of the table
          }else{
            let firstRowOfValues=0
            let stubHeader ="";
            eachTableInRow.forEach((rowOfTable,rowOfTableIndex) =>{                       
              if(rowOfTable.hasOwnProperty('row_type')){
                if(rowOfTable.row_type==='header'){
                  if(eachTableInRowIndex===0){
                    stubHeader=rowOfTable.stub_header;
                  }
                }
              }else{
                if(firstRowOfValues===0){
                  stubHeaderRow.push(<th key={'stubheader'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}  scope='row'>{stubHeader}</th>);
                  firstRowOfValues+=1;
                }
                
                if(eachTableInRowIndex===0){
                  stubValuesRow[rowOfTableIndex] = [];
                  stubValuesRow[rowOfTableIndex].push(<th key={'stubcategoryheader'+eachTableInRowIndex.toString()+rowOfTableIndex.toString()}  className="category-header">{rowOfTable.category_text}</th>);
                }
                rowOfTable.values.forEach((categoryCellValue,categoryCellValueIndex,categoryCellArray)=>{
                  let cellContents;
                  if(options.counts && !options.percentages){
                    if(categoryCellValue.counts){
                      cellContents=categoryCellValue.counts.toFixed(0);
                    }
                  }else if(options.percentages && !options.counts){
                    if(categoryCellValue.percentages){
                      cellContents=categoryCellValue.percentages.toFixed(0);
                    }
                  }else if(options.percentages && options.counts){
                    if(categoryCellValue.percentages){
                      cellContents=categoryCellValue.percentages.toFixed(0)+"%\r\n"+categoryCellValue.counts;
                    }
                  }
                  if(categoryCellArray.length-1===categoryCellValueIndex){
                    stubValuesRow[rowOfTableIndex].push(<td key={'stubcategorycell'+eachTableInRowIndex.toString()+categoryCellValueIndex.toString()+rowOfTableIndex.toString()} className='last-category-cell'>{cellContents}</td>);
                  }else{
                    stubValuesRow[rowOfTableIndex].push(<td key={'stubcategorycell'+eachTableInRowIndex.toString()+categoryCellValueIndex.toString()+rowOfTableIndex.toString()} className='category-cell'>{cellContents}</td>);
                  }
                })           
              }
            })
            //End of iterating each row of the table
          }
        })
        //End of iterating each table in the row of tables
        stubValuesRow.forEach((stubValues,stubValuesIndex) => {
          if(stubValues.length>0){
            stubValuesRows.push(<tr key={'stubvaluesrow'+eachRowOfTablesIndex.toString()+stubValuesIndex.toString()} className="category-row">{stubValues}</tr>);
          }
        });
        // console.log(stubValuesRows);
        tableHeader.push(<tr key={'bannerheaderrow'+eachRowOfTablesIndex.toString()}>{bannerHeaderRow}</tr>)
        tableHeader.push(<tr key={'bannercategoryheaderrow'+eachRowOfTablesIndex.toString()}>{bannerCategoryRow}</tr>)
        if(options.base){
          tableHeader.push(<tr key={'unweightedcategoryrow'+eachRowOfTablesIndex.toString()}>{unweightedBaseRow}</tr>)
        }
        if(options.wBase){
          tableHeader.push(<tr key={'weightedcategoryrow'+eachRowOfTablesIndex.toString()}>{weightedBaseRow}</tr>)
        }
        if (options.percentages && !options.counts){
          tableHeader.push(<tr key={'bannerpercentagesignrow'+eachRowOfTablesIndex.toString()}>{bannerPercentageSignRow}</tr>)
        }
        tableBody.push(<tr key={'stubheaderrow'+eachRowOfTablesIndex.toString()} className="stub-text-row">{stubHeaderRow}</tr>);
        tableBody.push(<>{stubValuesRows}</>);
        stubValuesRows = [];
      })
      //End of iterating each row of tables
      return (
        <div id="table_holder" className="table-responsive mx-3">
          <table className="table-sm">
            <thead>{tableHeader}</thead>
            <tbody>{tableBody}</tbody>
          </table>
        </div>
      );

    }else{
      return (
        <div id="table_holder" className="table-responsive mx-3">
          <table className="table-sm">
          </table>
        </div>
        );

    }

  }

}

export default MainTable
