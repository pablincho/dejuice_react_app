import { useDrag, useDrop } from 'react-dnd'
import EditableLabel from 'react-inline-editing';
import React, { useState, useRef } from 'react';
import  ItemTypes  from './ItemTypes'

const ModalRow = (props) => {
  const ref = useRef(null)
  const [, drop] = useDrop({
    accept: ItemTypes.ModalRow,
    hover(item, monitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = props.index
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }
      // Determine rectangle on screen
      const hoverBoundingRect = ref.current.getBoundingClientRect()
      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
      // Determine mouse position
      const clientOffset = monitor.getClientOffset()
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }
      // Time to actually perform the action
      if(!props.droppable){
        props.moveModalRow(dragIndex, hoverIndex)
      }
      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex
    },
  })
    const [{ isDragging }, drag] = useDrag({
        item: { type: ItemTypes.ModalRow, id: props.id , index: props.index },
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
        end: (item, monitor) => {
          const dropResult = monitor.getDropResult()
          if (item && dropResult) {
            if(dropResult.name === 'emptyRow'){
              props.editModalRow(item.id.text['en-GB'], props.mRValue.value)
            }
            // alert(`You dropped ${item.id.text['en-GB']} into ${dropResult.name}!`)
          }
        },
      })
    const opacity = isDragging ? 0 : 1;
    drag(drop(ref))
    return <tr ref={ref} style={{opacity}}>
        <th>{props.mRValue.value}</th>
        <th>{props.mREdit ? <EditableLabel text={props.mRValue.text['en-GB']}
                    labelClassName='m-0'
                    inputClassName='myInputClass'
                    inputWidth='150px'
                    inputHeight='20px'
                    inputMaxLength={50}
                    labelFontWeight='normal'
                    inputFontWeight='normal'
                    onFocusOut={(text) => {props.labelFocusOut(text, props.mRValueIdx)}}
                    /> : props.mRValue.text['en-GB']}
        </th>
        <th name='iconCol' style={{ maxWidth: '1rem',
                                              width: '1em',
                                              backgroundColor: 'white',
                                              borderTop: 'hidden',
                                              borderRight: 'hidden',
                                              borderBottom: 'hidden'}}>
            <i className="far fa-minus-square" style={{cursor: 'pointer'}} onClick={() => props.deleteRow(props.mRValue.value)}></i>
        </th>
    </tr>
}
export default ModalRow