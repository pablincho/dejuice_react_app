export default {
  
  variableIcon(type) {
    switch(type){
      case 'single': return 'far fa-check-circle mr-2'
      case 'delimited set': return 'far fa-check-square mr-2'
      case 'int': return 'fal fa-abacus mr-2'
      case 'string': return 'far fa-pencil mr-2'
      case 'date': return 'fal fa-calendar-alt mr-2'
      case 'float': return 'fal fa-calculator mr-2'
      default: break;
    }
  },
  checkVarNameLength(name){
    if(name.length > 20) {
      var newName = name.substring(0,13) + "...";
      return newName;
    }
    else{
      return name;
    }
  }
}
