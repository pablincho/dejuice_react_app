import React, { Component } from 'react'
import LayoutNavbar from './LayoutNavbar'
import LayoutSidenav from './LayoutSidenav'
import layoutHelpers from './helpers'
import Modal1 from './Modal'
import { cloneDeep } from 'lodash';
import { mainRoutes } from '../../routes';
import { Route } from 'react-router-dom'
import axios from 'axios'
class Layout1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      meta: null,
      data: [],
      modalState: false,
      modalType: '',
      variableSet: null,
      columns: [],
      tables: [],
      stubVars: [],
      bannerVars: [],
      floatVars: [],
      showTable: false,
      cloneFromTable: '',
      cloneInitialValue: '',
      saveInitialValue: '',
      cloneOptions: [],
      saveOptions: [],
      weightVarsList: [],
      numericVars: [],
      numericVarInitialValue: '',
      currentTable: 0,
      currentOptions: {
        percentages: false,
        counts: true,
        wVar: '',
        total: true,
        base: true,
        wBase: true
      }
    }

    this.mainRoutes = mainRoutes.map(route => {
      // route.layout = route.layout || DefaultLayout
      route.exact = typeof route.exact === 'undefined' ? true : route.exact
      return route
    })
    // This binding is necessary to make `this` work in the callback
    this.getMetaData = this.getMetaData.bind(this);
    this.sumWeights = this.sumWeights.bind(this);
    // this.handleUpload = this.handleUpload.bind(this);
  }
  closeSidenav(e) {
    e.preventDefault()
    layoutHelpers.setCollapsed(true)
  }

  getMetaData = (metaFromChild, dataFromChild) => {
    let columnsArr = Object.values(metaFromChild.columns).map(column =>
      {
        return {name: column.name, type: column.type}
      })
    console.log(columnsArr)
    const table = [{
      tableName: "Table 1",
      tableIndex: 1,
      tableContent: {stubVars: [], bannerVars: []},
      tableOptions: {
        percentages: false,
        counts: true,
        wVar: '',
        total: true,
        base: true,
        wBase: true
      },
    }]
    const numericVars = Object.values(metaFromChild.columns).filter(column =>{
      return column.type === 'int';
    })
    const numericVarInitialValue = !!numericVars.length ? numericVars[0].name : '';
    this.setState({
      meta: metaFromChild,
      data: dataFromChild,
      columns: columnsArr,
      tables: table,
      numericVars,
      numericVarInitialValue,
    });
  }

  showModal = (modalState, from, modalType) => {
    switch(modalType){
      case 'variables':
        if(from){
          this.setState((prevState) => {
            const varSet = {...prevState}
            const currentState = varSet.meta.columns[from]
            return {variableSet: currentState, modalState, modalType}
          })
        } break;
      case 'save table':
        this.setState(prevState => {
          const tables = prevState.tables
          const initialValue = tables.length > 0 ? tables[0].tableName : ''
          return({modalState, modalType, saveOptions: tables, saveInitialValue: initialValue})
        })
        break;
      case 'options':
        this.setState({modalState, modalType})
        break;
      case 'clone table':
        this.setState(prevState => {
          const tables = prevState.tables
          const options = tables.filter(element => element.tableName !==from.tableName)
          const initialValue = options.length > 0 ? options[0].tableName : ''
          return({modalState, modalType, cloneOptions: options, cloneInitialValue: initialValue, cloneFromTable: from.tableName})
        })
        break; 
      case 'script vars':
        this.setState({modalState, modalType})
        break;   
      default:
        this.setState({modalState})
        break;
    }
  }

  enableShowTable = () => {
    this.setState({showTable: true})
  }

  saveNewVar = (variable, modalState) => {
    this.setState((prevState) => {
      const originalVar = this.state.variableSet.name;
      const metaVar = {...prevState.meta}
      const currentState = cloneDeep(metaVar)
      const dataVar = prevState.data
      let finder = prevState.columns.filter(element => element.name === variable.name)
      var name;
      if(finder.length > 0){
        name = variable.name + '*'
        variable.name = variable.name + '*'
      }
      else{
        name = variable.name
      }
      currentState.columns[name] = variable;
      currentState.sets['data file'].items.push('columns@' + name);
      dataVar.forEach(element => {element[name] = element[originalVar]})
      const columnsVar = prevState.columns.concat({name, type: variable.type});
      return {meta: currentState, columns: columnsVar, modalState}
    })
  }
  dragVar = (name, type, sendTo) => {
    this.setState(prevState => {
      if(sendTo === 'StubList'){
          const stubVars = prevState.stubVars.concat({name, type});
          console.log(stubVars)
          return {stubVars}
        }
      if(sendTo === 'BannerList'){
        const bannerVars = prevState.bannerVars.concat({name, type});
        return {bannerVars}
      }
      if(sendTo === 'WeightingList'){
        const wBData = prevState.data.map(element => element[name]);
        const weightValues = Object.assign({}, prevState.meta.columns[name].values.map(() => 0));
        const weightVar = {...prevState.meta.columns[name], meta:prevState.meta.columns[name], weightTotal: 0, weightValues, data: wBData};
        const weightVarsList = prevState.weightVarsList.concat(weightVar);
        return {weightVarsList}
      }
      // if(sendTo === 'SpecList'){
      //   const wBData = prevState.data.map(element => element[name]);
      //   const weightVar = {...prevState.meta.columns[name], meta:prevState.meta.columns[name], weightTotal: 0, weightValues: {}, data: wBData};
      //   const weightVarsList = prevState.weightVarsList.concat(weightVar);
      //   return {weightVarsList}
      // }

    });
  }

  deleteDraggedWeightVar = (varName) => {
    this.setState(prevState => {
      const weightVarsList = prevState.weightVarsList.filter(weightVar => weightVar.name !== varName);
      return {weightVarsList}
    })
  }

  addNumericVar = (e) => {
    const numericVarInitialValue = e.target.value
    this.setState({numericVarInitialValue})
  }

  sumWeights = (wLIdx, e, wVIdx) => {
    const inputValue = e.target.value;
    this.setState(prevState => {
      const weightVarsList = prevState.weightVarsList
      weightVarsList[wLIdx].weightValues[wVIdx] = Math.round(Number(inputValue) * 10) / 10;
      let sumValues = Object.values(weightVarsList[wLIdx].weightValues);
      weightVarsList[wLIdx].weightTotal = sumValues.reduce((acc, currentValue) =>  Math.round(Number(acc) * 10) / 10 + Math.round(Number(currentValue) * 10) / 10 )
      weightVarsList[wLIdx].calcWeightAllowed = weightVarsList[wLIdx].weightTotal === 100 ? true : false;
      return {weightVarsList}
    })
  }

  calculateWeight = () => {
    let numericVar = {}
    const existingMeta = {...this.state.meta}
    const existingData = Array.from(this.state.data)
    numericVar.meta = this.state.numericVarInitialValue ? existingMeta.columns[this.state.numericVarInitialValue] : {};
    numericVar.data = existingData.map(element => element[this.state.numericVarInitialValue])
    let data = {
      weightVarsList: this.state.weightVarsList,
      numericVar
    }
    axios({
      // baseURL: "http://localhost:5000",
      baseURL: "https://api.spectabulate.com/",
      url: "/weightsBuilder",
      method: "POST",
      headers: { 'content-type': 'application/json' },
      data: data
    }).then(res => {
      console.log(res.data);
      this.handleDataWeighting(res.data)
    }, err => {console.log(err)});
  }

  handleDataWeighting(res){
    this.setState((prevState) => {
      const existingMeta = {...prevState.meta};
      const existingData = Array.from(prevState.data);
      let weightVarName='djW8_0';
      if(existingMeta.columns[weightVarName]){
        console.log('weight var exists')
        let weightVarNameElements = weightVarName.split('_');
        let weightVarNumber = parseInt(weightVarNameElements[1])+1;
        if(existingMeta.columns[weightVarNameElements[0]+"_"+weightVarNumber.toString()]){
          weightVarNumber+=1;
        }else{
          weightVarName=weightVarNameElements[0]+"_"+weightVarNumber.toString();
        }
      }else{
        let newWeightVar = {};
        newWeightVar.name = weightVarName;
        newWeightVar.parent= {};
        newWeightVar.text = {};
        newWeightVar.type="float";
        newWeightVar.text['en-GB'] = 'Dejuice Raked Weight';
        existingMeta.columns[weightVarName]=newWeightVar;
        existingMeta.sets['data file'].items.push("columns@"+weightVarName);
      }
      const existingData1 = existingData.map((d, dIndex) => {
        d[weightVarName] = res[dIndex][0];
        return d
      })

      return {meta: existingMeta, data: existingData1}
      
    })
    this.getMetaData(this.state.meta, this.state.data);
  }

  deleteDraggedVar = (draggedVarIndex, from) => {
    this.setState(prevState => {
      if(from === 'stubList'){
        const stubVars = prevState.stubVars.filter((item, idx) => draggedVarIndex !== idx);
        return {stubVars}
      }
      if(from === 'bannerList'){
        const bannerVars = prevState.bannerVars.filter((item, idx) => draggedVarIndex !== idx);
        return {bannerVars}
      }
    })
  }
  updateOptions = (options, modalState) => {
    const newOptions = {...options};
    this.setState(prevState => {
      let currentTable = prevState.currentTable
      const tables = prevState.tables.map((item, idx) => {
        if(idx === currentTable){
          let newItem = {...item};
          newItem.tableOptions = newOptions;
          return newItem
        }
        else{
          return item
        }
      })
      const currentOptions = newOptions;
      return {tables, modalState, currentOptions}
    })
  }
  addTable = () => {
    this.setState(prevState => {
      if(prevState.tables.length > 0){
        const lastIndex = prevState.tables.length - 1;
        const tableNumber = prevState.tables[lastIndex].tableIndex;
        const tables = prevState.tables.concat({
          tableName: `Table ${tableNumber + 1}`,
          tableIndex: tableNumber + 1,
          tableContent: {stubVars: [], bannerVars: []},
          tableOptions: {
            percentages: false,
            counts: true,
            wVar: '',
            total: true,
            base: true,
            wBase: true
          }
        })
        const cloneFromTable = prevState.cloneFromTable
        const saveInitialValue = tables[0].tableName
        const cloneOptions = tables.filter(element => element.tableName !==cloneFromTable)
        const cloneInitialValue = cloneOptions.length > 0 ? cloneOptions[0].tableName : null
        return {tables, saveOptions: tables, saveInitialValue, cloneOptions, cloneInitialValue }
      }
    })
  }
  deleteTable = (tableIndex) => {
    this.setState(prevState => {
      const tables = prevState.tables.filter((element, idx) => idx !== tableIndex);
      let currentTable;
      if(tableIndex === prevState.currentTable){
        if(tableIndex === 0){
          currentTable = tableIndex;
        }
        else{
          currentTable = tableIndex - 1 ;
        }
      }
      else{
        if(tableIndex < prevState.currentTable){
          currentTable = prevState.currentTable - 1;
        }
        else{
          currentTable = prevState.currentTable;
        }
      }
      const currentOptions = tables[currentTable].tableOptions;
      return { tables, currentTable, currentOptions }
    })
  }

  saveToTable = () => {
    this.setState(prevState => {
      if(prevState.tables.length > 0){
        const initialValue = prevState.saveInitialValue
        const currentTables = prevState.tables;
        const index = currentTables.findIndex(element => element.tableName === initialValue);
        currentTables[index].tableContent.stubVars = prevState.stubVars
        currentTables[index].tableContent.bannerVars = prevState.bannerVars
        return {tables: currentTables, modalState: false}
      }
    })
  }

  cloneTable = () => {
    this.setState(prevState => {
      if(prevState.tables.length > 0){
        const initialValue = prevState.cloneInitialValue
        const currentTables = prevState.tables;
        const cloneFromTable = prevState.cloneFromTable;
        const indexTo = currentTables.findIndex(element => element.tableName === initialValue);
        const indexFrom = currentTables.findIndex(element => element.tableName === cloneFromTable)
        const stubVarsToClone = currentTables[indexFrom].tableContent.stubVars
        const bannerVarsToClone = currentTables[indexFrom].tableContent.bannerVars
        currentTables[indexTo].tableContent.stubVars = stubVarsToClone
        currentTables[indexTo].tableContent.bannerVars = bannerVarsToClone
        return {tables: currentTables, modalState: false}
      }
    })
  }

  changeSaveValue = (value) => {
    this.setState({saveInitialValue: value})
  }
  
  changeCloneValue = (value) => {
    this.setState({cloneInitialValue: value})
  }

  showSavedVars = (table, tableIdx ) => {
    this.setState({
      stubVars: table.tableContent.stubVars, 
      bannerVars: table.tableContent.bannerVars, 
      drawTable: true,
      currentTable: tableIdx,
      currentOptions: table.tableOptions
    })
  }
  scrollTop(to, duration, element = document.scrollingElement || document.documentElement) {
    if (element.scrollTop === to) return
    const start = element.scrollTop
    const change = to - start
    const startDate = +new Date()

    if (!duration) {
      element.scrollTop = to
      return
    }

    // t = current time; b = start value; c = change in value; d = duration
    const easeInOutQuad = (t, b, c, d) => {
      t /= d / 2
      if (t < 1) return c / 2 * t * t + b
      t--
      return -c / 2 * (t * (t - 2) - 1) + b
    }

    const animateScroll = () => {
      const currentDate = +new Date()
      const currentTime = currentDate - startDate
      element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration))
      if (currentTime < duration) {
        requestAnimationFrame(animateScroll)
      } else {
        element.scrollTop = to
      }
    }
    animateScroll()
  }
  render() {
    if(this.state.meta && this.state.variableSet){
      var modalBody = cloneDeep(this.state.variableSet)
    }
    return (
      <div className="layout-wrapper layout-1">
        <Modal1 modalState={this.state.modalState} modalInfo={this.showModal}
        modalBody={modalBody} saveVar={this.saveNewVar} editMode={false} modalType={this.state.modalType}
        columns={this.state.columns} options={this.state.currentOptions} updateOpts={this.updateOptions} tables={this.state.tables}
        addTable={this.addTable} saveToTable={this.saveToTable} cloneFromTable={this.state.cloneFromTable} cloneTable={this.cloneTable}
        saveInitialValue={this.state.saveInitialValue} cloneInitialValue={this.state.cloneInitialValue}
        saveOptions={this.state.saveOptions} cloneOptions={this.state.cloneOptions}
        changeSaveValue={this.changeSaveValue} changeCloneValue={this.changeCloneValue}/>
        <div className="layout-inner">
          <LayoutNavbar {...this.props} cbFromParent = { this.getMetaData }/>

          <div className="layout-container">
            <LayoutSidenav {...this.props}
            sendMeta = {this.state.columns}
            modalInfo={this.showModal}
            sendTables = {this.state.tables} showModal={this.showModal}
            dragVar={this.dragVar} addTable={this.addTable} deleteTable={this.deleteTable}
            showSavedVars={this.showSavedVars}
            currentTable={this.state.currentTable}/>

            <div className="layout-content m-0" style={{backgroundColor: '#fff'}}>
              <div className="container-fluid flex-grow-1 container-p-y">
                {this.mainRoutes.map(route => (
                  <Route
                    path={route.path}
                    exact={route.exact}
                    render={props => {
                      // On small screens collapse sidenav
                      if (window.layoutHelpers && window.layoutHelpers.isSmallScreen()) {
                        window.layoutHelpers.setCollapsed(true, false)
                      }
                      // Scroll page to top on route render
                      // this.scrollTop(0, 0)
                      // Return component
                      return <route.component {...props} stubVars={this.state.stubVars}
                      bannerVars={this.state.bannerVars}
                      weightVarsList={this.state.weightVarsList}
                      deleteDraggedVar={this.deleteDraggedVar}
                      deleteDraggedWeightVar={this.deleteDraggedWeightVar}
                      sumWeights={this.sumWeights}
                      addNumericVar={this.addNumericVar}
                      calculateWeight={this.calculateWeight}
                      enableShowTable={this.enableShowTable}
                      showTable={this.state.showTable}
                      meta={this.state.meta}
                      data={this.state.data}
                      showModal={this.showModal}
                      numericVars={this.state.numericVars}
                      numericVarInitialValue={this.state.numericVarInitialValue}
                      options={this.state.currentOptions}
                      currentTableObj={this.state.tables[this.state.currentTable]}/>
                    }}
                    key={route.path}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="layout-overlay" onClick={this.closeSidenav}></div>
      </div>
    )
  }
}

export default Layout1
