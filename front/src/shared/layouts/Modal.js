import React, { Component } from 'react'
import {Button, Modal, Row, Col, Form} from 'react-bootstrap'
import { cloneDeep } from 'lodash';
import EditableLabel from 'react-inline-editing';
import ModalRow from './ModalRow';
import ModalDroppableRow from './ModalDroppableRow';
import update from 'immutability-helper';
import {UnControlled as CodeMirror} from 'react-codemirror2'
import 'codemirror/lib/codemirror.css'

class Modal1 extends Component {
  constructor(props) {
    super(props)
    this.instance=null
    this.state = {
      title: '',
      description: '',
      body: null,
      edit: false,
      rows: [],
      droppableRow: false,
      options: {
        percentages: false,
        counts: true,
        wVar: '',
        total: true, 
        base: true, 
        wBase: true
      },
      saveTablesValue: null,
      cloneTablesValue: null,
      cloneTables: [],
      codeValue: null,
      cMValue: 'console.log("Hello World")',
      runCode: false,
    }
    // This binding is necessary to make `this` work in the callback
    this.addCategory = this.addCategory.bind(this);
    this._handleFocusOut = this._handleFocusOut.bind(this);
    this.toggleSortEdit = this.toggleSortEdit.bind(this);
    this.moveModalRow = this.moveModalRow.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
  }

  runCode = () => {
    // this.setState({runCode: true})
    var js = this.instance.getValue();
    console.log(js)
    var s = document.createElement('script');
    s.textContent = js;//inne
    document.body.appendChild(s);
  }

  _handleFocusOut = (text, from) => {
    this.setState( prevState => {
      const currentState = {...prevState};
      if(from === 'title'){
        currentState.body.name = text;
      }
      else if(from === 'description'){
        currentState.body.text['en-GB'] = text;
      }
      else{
        currentState.body.values[from].text['en-GB'] = text;
      }
      return {body: currentState.body}
    })
  }

  componentDidUpdate(prevProps){
    if (this.props.modalBody !== prevProps.modalBody) {
      const rowValues = this.props.modalBody.values ? this.props.modalBody.values : [];
      this.setState({ body: this.props.modalBody, edit: this.props.editMode, 
        rows: rowValues });
    }
    if (this.props.options !== prevProps.options) {
      const options = this.props.options;
      this.setState({ options });
    }
    // this.instance.refresh();
    // this.instance.focus();  
  }

  handleCheckBoxChange(e){
    const checked = e.target.checked
    const id = e.target.id;
    this.setState(prevState => {
      const options = prevState.options;
      switch (id){
        case 'setPercentages':
          options.percentages = checked
          return {options}
        case 'setCounts':
          options.counts = checked
          return {options}
        case 'checkTotal':
          options.total = checked
          return {options}
        case 'checkBase':
          options.base = checked
          return {options}
        case 'checkWBase':
          options.wBase = checked
          return {options}
      }

    })
  }

  handleSelectChange(e){
    const id = e.target.id;
    const value = e.target.value
    this.setState(prevState => {
      switch(id){
        case 'wVarsSelect':
          const options = prevState.options;
          options.wVar = value;
          return {options}
        case 'saveTablesSelect':
          this.props.changeSaveValue(value)
          break;
          // return {saveTablesValue: value}
        case 'cloneTablesSelect':
          this.props.changeCloneValue(value)
          break;
          // return {cloneTablesValue: value}  
        }
    })
  }

  addCategory(){
    this.setState((prevState) => {
      let lastLength = prevState.body.values.length;
      let droppableProp = prevState.rows.findIndex(element => element.droppable)
      if(lastLength > 0){
        var lastValue = prevState.body.values[lastLength - 1].value;
        if(prevState.body.values[lastLength - 1].text['en-GB'] !== '' && droppableProp === -1){
          const rows = prevState.rows.concat({text: {['en-GB']: ''}, value: lastValue + 1, droppable: true, combined: true, from: []})
          const newValues = prevState.body.values.concat({text: {['en-GB']: ''}, value: lastValue + 1, combined: true, from: []})
          const currentState = cloneDeep(prevState)
          currentState.body.values = newValues
          return {body: currentState.body, rows, droppableRow: true}
        }
      }
      else{
        if(droppableProp === -1){
          lastValue = 0;
          const rows = prevState.rows.concat({text: {['en-GB']: ''}, value: lastValue + 1, droppable: true})
          const newValues = prevState.body.values.concat({text: {['en-GB']: ''}, value: lastValue + 1})
          const currentState = cloneDeep(prevState)
          currentState.body.values = newValues
          return {body: currentState.body, rows}
        }
      }
      
    })
  }
  toggleSortEdit(){
    this.setState((prevState) => {
      const edit = prevState.edit;
      return {edit: !edit}
    })
  }

  editModalRow = (value, indexValue) => {
    this.setState(prevState => {
      const rows = prevState.rows
      const body = {...prevState.body}
      if(!!rows[rows.length - 1].text['en-GB']){
        rows[rows.length - 1].text['en-GB'] += ` - ${value}`;
        rows[rows.length - 1].from.push(indexValue);
      }
      else{
        rows[rows.length - 1].text['en-GB'] = value;
        rows[rows.length - 1].from.push(indexValue);
      }
      body.values = rows;
      return{rows, body}
    })
  }

  deleteModalRow = (value) => {
    this.setState((prevState) => {
      const currentState = {...prevState};
      let deleteIndex = currentState.rows.findIndex(element => element.value == value)
      currentState.rows.splice(deleteIndex, 1)
      currentState.body.values = currentState.rows
      return {body: currentState.body, rows: currentState.rows}
    })
  } 
  moveModalRow(dragIndex, hoverIndex) {
    const rows = cloneDeep(this.state.rows);
    const currentState = cloneDeep(this.state.body);
    const dragRow = rows[dragIndex];
    // this.setState({ dragIndex, hoverIndex });
    currentState.values = 
    this.setState(
      update(this.state, {
        rows: {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]],
        },
        body: {values: {$splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]]}}
      }),
    );
  }
  setDroppable = (value) => {
    this.setState(prevState => {
      const rows = prevState.rows;
      delete rows[rows.length - 1].droppable;
      return {droppableRow: value, rows}
    })
  }

  render() {
    let CMOptions = {
      theme: 'neat',
      lineNumbers: true,
      autoRefresh:true,
      mode: "Plain Text",
      matchBrackets: true
		};
    // const cloneFromTable = this.props.cloneFromTable
    // let cloneTables = this.props.tables.filter(element => element.tableName !== cloneFromTable.tableName)
    const options = this.props.options;
    let wVars = this.props.columns.filter(element => element.type === 'float')
    let modalType = this.props.modalType;
    let title, body, footer;
    var categories = this.state.rows.map((value, valueIdx) => {
      if(value.droppable){
        var droppable = value.droppable;
      }
      if(droppable){
        return <ModalDroppableRow key={value.text['en-GB'] + 'droppable'} 
          index={valueIdx}
          id={value}
          moveModalRow={this.moveModalRow}
          mRValue={value} mRValueIdx={valueIdx} mREdit={this.state.edit}
          deleteRow={this.deleteModalRow} labelFocusOut={this._handleFocusOut}
          droppable={this.state.droppableRow}
          setDroppable={this.setDroppable}
          editModalRow={this.editModalRow}
        />
      }
      else{
        return <ModalRow key={value.text['en-GB']} 
        index={valueIdx}
        id={value}
        moveModalRow={this.moveModalRow}
        mRValue={value} mRValueIdx={valueIdx} mREdit={this.state.edit}
        deleteRow={this.deleteModalRow} labelFocusOut={this._handleFocusOut}
        droppable={this.state.droppableRow}
        editModalRow={this.editModalRow}
      />  
      }
    });

    switch(modalType){
      case 'variables':
        title = <EditableLabel text={this.props.modalBody.name}
        labelClassName='pointer'
        inputClassName='pointer'
        inputWidth='200px'
        inputHeight='25px'
        inputMaxLength={50}
        labelFontWeight='bold'
        inputFontWeight='bold'
        onFocus={this._handleFocus}
        onFocusOut={(text) => {this._handleFocusOut(text, 'title')}}
        />
        body = <><EditableLabel text={this.props.modalBody.text['en-GB']}
          labelClassName='pointer mb-4'
          inputClassName='pointer'
          inputWidth='500px'
          inputHeight='70px'
          inputMaxLength={500}
          labelFontWeight='bold'
          inputFontWeight='bold'
          onFocus={this._handleFocus}
          onFocusOut={(text) => {this._handleFocusOut(text, 'description')}}
          />
          {this.state.rows.length > 0 ? <Row>
            <Col className="col-sm mx-4">
              <div className="table-wrap">
                <div className="table-responsive px-4">
                  <table className="table table-bordered table-striped mb-0">
                    <thead>
                      <tr>
                        <th>Value</th>
                        <th>Text</th>
                      </tr>
                    </thead>
                    <tbody>
                      {categories}
                    </tbody>
                  </table>
                </div>
              </div>
            </Col>
          </Row> : 'No categories'}
          <div className="row justify-content-end px-3 my-2">
            <div className="col-md text-right"><i id="addCategory" style={{cursor: 'pointer'}} className="fas fa-plus-square" onClick={this.addCategory}></i></div>
          </div>
        </>
        footer = <><Button variant="secondary" onClick={()=>this.props.modalInfo(false)}>
                      Close
                  </Button>
                  <Button variant="secondary" onClick={this.toggleSortEdit}>
                    {this.state.edit ? 'Sort' : 'Edit'}
                  </Button>
                  <Button variant="primary" onClick={()=>this.props.saveVar(this.state.body, false)}>
                    Save to new variable
                  </Button></>
        break;
      case 'options':
        title = 'Set table options';
        body = <>
          <div className="modal-body pt-0 pl-1">
            <div className="row justify-content-start">
              <div className="col">
                Select table cell type
              </div>
            </div>
            <div className="row justify-content-start my-2">
              <div className="col-md-6">
                <div className="mb-1">
                  <Form.Check type={'checkbox'} id={'setPercentages'} className='mb-0'>
                    <Form.Check.Input type={'checkbox'} onChange={(e) => this.handleCheckBoxChange(e)} defaultChecked={options.percentages}/>
                    <Form.Check.Label>{'Percentages'}</Form.Check.Label>
                  </Form.Check>
                </div>
                <div className="mb-1">
                  <Form.Check type={'checkbox'} id={'setCounts'} className='mb-0'>
                    <Form.Check.Input type={'checkbox'} onChange={(e) => this.handleCheckBoxChange(e)} defaultChecked={options.counts} />
                    <Form.Check.Label>{'Counts'}</Form.Check.Label>
                  </Form.Check>
                </div>
              </div>
            </div>
            <div className="row justify-content-start">
              <div className="col">
                Select weighting variable
              </div>
            </div>
            <div className="row justify-content-start my-2">
              <div className="col-md-5 text-center">
              <Form>
                <Form.Group controlId="wVarsSelect">
                  <Form.Control as="select" size="sm" onChange={(e) => this.handleSelectChange(e)} defaultValue={options.wVar} custom>
                    <option>{''}</option>
                    {!!wVars ? wVars.map((element, idx) => {
                      return <option key={idx}>{element.name}</option>
                    }) : null}    
                  </Form.Control>
                </Form.Group>
              </Form>
            </div>
            </div>
            <div className="row justify-content-start">
              <div className="col">
                Table display options
              </div>
            </div>
            <div className="row justify-content-start my-2">
              <div id="optionsModalContent" className="col-md-6">
                <div className="mb-1">
                    <Form.Check type={'checkbox'} id={'checkTotal'} className='mb-0'>
                      <Form.Check.Input type={'checkbox'} onChange={(e) => this.handleCheckBoxChange(e)} defaultChecked={options.total}/>
                      <Form.Check.Label>{'Total'}</Form.Check.Label>
                    </Form.Check>
                  </div>
                  <div className="mb-1">
                    <Form.Check type={'checkbox'} id={'checkBase'} className='mb-0'>
                      <Form.Check.Input type={'checkbox'} onChange={(e) => this.handleCheckBoxChange(e)} defaultChecked={options.base}/>
                      <Form.Check.Label>{'Base'}</Form.Check.Label>
                    </Form.Check>
                  </div>
                  <div className="mb-1">
                    <Form.Check type={'checkbox'} id={'checkWBase'} className='mb-0'>
                      <Form.Check.Input type={'checkbox'} onChange={(e) => this.handleCheckBoxChange(e)} defaultChecked={options.wBase}/>
                      <Form.Check.Label>{'Weighted base'}</Form.Check.Label>
                    </Form.Check>
                  </div>
              </div>
            </div>
          </div>
        </>
        footer = <><Button variant="secondary" onClick={()=>this.props.modalInfo(false)}>
                    Cancel
                  </Button>
                  <Button variant="primary" onClick={()=>this.props.updateOpts(this.state.options, false)}>
                    Save changes
                  </Button></>
        break;
      case 'save table':
        title = 'Save variable'
        body = <div className="modal-body">
                <div className="row justify-content-start">
                  <div className="col">
                    Select table to save variable to:
                  </div>
                </div>
                <div className="row justify-content-center my-5">
                  <div id="tablesModalContent" className="col-md-6 text-center">
                    <div className="form-group">
                    <Form>
                      <Form.Group controlId="saveTablesSelect">
                        <Form.Control as="select" onChange={(e) => this.handleSelectChange(e)} value={this.props.saveInitialValue} custom>
                          {this.props.saveOptions.map((element, idx) => {
                            return <option key={idx}>{element.tableName}</option>
                          })}    
                        </Form.Control>
                      </Form.Group>
                    </Form>
                    </div>
                  </div>
                </div>
              </div>
        footer =<><Button variant="secondary" onClick={()=>this.props.modalInfo(false)}>
                  Cancel
                  </Button>
                  <Button variant="secondary" onClick={() => this.props.addTable()}>
                    Add new table
                  </Button>
                  <Button variant="primary" onClick={()=>this.props.saveToTable()}>
                    Save
                  </Button></>
        break;
      case 'clone table':
        title = 'Clone table'
        body = <div className="modal-body">
                <div className="row justify-content-start">
                  <div id="cloneTablesModalDescription" className="col">
                  </div>
                </div>
                <div className="row justify-content-center my-5">
                  <div id="cloneTablesModalContent" className="col-md-6 text-center">
                    <div className="form-group">
                    <Form>
                      <Form.Group controlId="cloneTablesSelect">
                        <Form.Control as="select" onChange={(e) => this.handleSelectChange(e)} value={this.props.cloneInitialValue} custom>
                          {this.props.cloneOptions.map((element, idx) => {
                            return <option key={idx}>{element.tableName}</option>
                          })}    
                        </Form.Control>
                      </Form.Group>
                    </Form>
                    </div>
                  </div>
                </div>
              </div>
        footer = <><Button variant="secondary" onClick={()=>this.props.modalInfo(false)}>
        Cancel
        </Button>
        <Button variant="secondary" onClick={() => this.props.addTable()}>
          Add new table
        </Button>
        <Button variant="primary" onClick={()=>this.props.cloneTable()} disabled={!this.props.cloneOptions.length}>
          Clone
        </Button></>
        break;
      case 'script vars':
        title = 'Variable Script Editor';
        body = <CodeMirror style={{backgroundColor: 'red'}}
        // value={this.state.cMValue}
        options={{
          theme: 'neat',
          lineNumbers: true,
          autoRefresh:true,
          mode: "Plain Text",
          matchBrackets: true,
          autofocus: true
        }}
        onBeforeChange={(editor, data, value) => {
          editor.refresh()
          this.setState({codeValue: value});
        }}
        onChange={(editor, data, value) => {
          this.setState({
            runCode: false,
            cMValue: value,
          })
        }
        }
        editorDidMount={(editor) => {
          // editor.refresh()
          this.instance = editor
          }
        }
      
      />
        footer = <><Button variant="secondary" onClick={()=>this.props.modalInfo(false)}>
        Cancel
        </Button>
        <Button variant="primary" onClick={this.runCode}>
          Run Code
        </Button></>
        break;
      default:
        break;
    }
    
    return (
      <Modal show={this.props.modalState} onHide={()=>this.props.modalInfo(false)}>
        <Modal.Header closeButton>
          <Modal.Title>
            {title}
          </Modal.Title>
        </Modal.Header>
          <Modal.Body>
            {body}
          </Modal.Body>
        <Modal.Footer>
          {footer}
        </Modal.Footer>
      </Modal>
    )
  }
}
export default Modal1
