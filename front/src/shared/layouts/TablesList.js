import React, { Component } from 'react'
import layoutHelpers from './helpers'
import {Row, Col} from 'react-bootstrap'
import '../../css/custom.css'
import FeatherIcon from 'feather-icons-react';

class TablesList extends Component {
  constructor(props) {
    super(props);

}
  closeSidenav(e) {
    e.preventDefault()
    layoutHelpers.setCollapsed(true)
  }

  componentDidMount() {
    layoutHelpers.init()
    layoutHelpers.update()
    layoutHelpers.setAutoUpdate(true)
  }

  componentWillUnmount() {
    layoutHelpers.destroy()
  }
 
  render() {
    const currentTable = this.props.currentTable;
    var tables = this.props.sendTables.map((table, tableIdx) => {
      return <li key={table.tableName}>
        <div className={'nav-link d-flex align-items-center justify-content-between py-0' + (currentTable === tableIdx ? ' selected' : '')}
         style={{height: '2.7rem'}}>
          <div className="py-0 h-100 d-flex">
            <div className="p-0 d-flex h-100 align-self-center" onClick={() => this.props.showSavedVars(table, tableIdx)}>
              <i className="fas fa-table mr-2 align-self-center"></i>
              <span className="mr-3 align-self-center">{table.tableName}</span>
            </div>
            {table.tableIndex > 1 ? <i className="fal fa-trash-alt mr-2 align-self-center" style={{zIndex: 100}} onClick={() => this.props.deleteTable(tableIdx)}></i> : null}
            <i className="far fa-clone align-self-center" onClick={() => this.props.showModal(true, table, 'clone table')}></i>
          </div>
        </div>
      </li>
    })
    return (
      <Row style={{width: '100%'}}>
        <Col className="pr-0">
        <h6 className="sidebar-heading d-flex justify-content-between align-items-center pl-3 mt-2 mb-1 text-muted">
          <span>Tables List</span>
          <FeatherIcon icon="plus-circle" style={{cursor: 'pointer'}}  onClick={() => this.props.addTable()}/>
        </h6>
        <ul className="nav flex-column mb-2 sidebar">
        {tables}
        </ul>
        <div className={`sidenav-inner ${this.props.orientation !== 'horizontal' ? 'py-1' : ''}`}>
        </div>
        </Col>
      </Row>
    )
  }
}
export default TablesList
