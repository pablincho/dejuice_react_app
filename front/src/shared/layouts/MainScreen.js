import React, { Component } from 'react'
import StubList from './StubList'
import BannerList from './BannerList'
import MainTable from './MainTable'

class MainScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tableResult: [],
      
    }
    this.enablePrintTable = this.enablePrintTable.bind(this);
  }

  enablePrintTable = () => {
    this.refs.mainTableChild.printTable();
  }

  componentDidUpdate(prevProps){
  }

  render() {
    console.log(this.props)
    return (
      <div style={{fontFamily: "'Lexend Deca', sans-serif"}}>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 className="h2">Table Builder</h1>
          <div className="btn-toolbar mb-2 mb-md-0">
            <button id="print_table" style={{display:this.props.showTable ? 'block' : 'none'}} className="btn btn-filled btn-outline-secondary btn-filled-secondary p-2" onClick = {this.enablePrintTable}>
            <i className="far fa-file-excel mr-1"></i>Print Table
            </button>
            <button id="scriptVariables" className="btn btn-filled btn-outline-secondary btn-filled-secondary p-2" onClick={()=> this.props.modalInfo(true, null, 'script vars')}>
              <i className="far fa-brackets-curly mr-1"></i>
              Script Variables
            </button>
            <button id="saveTable" className="btn btn-filled btn-outline-secondary btn-filled-secondary p-2" onClick={()=> this.props.modalInfo(true, null, 'save table')}>
              <i className="far fa-share-square mr-1"></i>
              Save Table
            </button>
            <button id="run_table" className="btn btn-outline-secondary btn-filled-secondary p-2" onClick = {() => {this.props.enableShowTable()}}>
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-play-circle"><circle cx="12" cy="12" r="7"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg>
              Calculate Table
            </button>
          </div>
        </div>
        <div id="table_top" className="row mt-3">
          <div className="col-12 col-sm-6 col-md-3 col-xl-2 grid-margin stretch-card">
            <div className="card h-100">
              <div className="card-body">
                <h6 className="card-title">Table Cell Options</h6>
                <div className="d-flex justify-content-between align-items-center">
                  <span style={{textDecoration: 'none', color: 'black', cursor: 'pointer'}}><i className="fas fa-filter"
                  onClick={() => this.props.modalInfo(true, this.props.name, 'options')}></i></span>
                </div>
                {/* <canvas id="customer" height="47" style="display: block; width: 239px; height: 47px;" width="239" className="chartjs-render-monitor"></canvas> */}
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-9 col-xl-10 grid-margin stretch-card">
            <BannerList bannerVars={this.props.bannerVars} deleteDraggedVar = {this.props.deleteDraggedVar}/>
          </div>
        </div>
        <div id="table_bottom" className="row mt-3" style={{minHeight: '50vh'}}>
          <div id="stub_div" className="col-12 col-sm-6 col-md-3 col-xl-2 grid-margin stretch-card">
            <StubList stubVars={this.props.stubVars} deleteDraggedVar = {this.props.deleteDraggedVar}/>
          </div>
          <div id="table_div" className="col-12 col-sm-6 col-md-9 col-xl-10 grid-margin stretch-card h-100">
            <div id="table_card" className="card h-100" style={{minHeight: '50vh'}}>
              <div className="card-body">
                <h6 className="card-title">{this.props.currentTableObj ? this.props.currentTableObj.tableName : 'Table'}</h6>
                
                {this.props.showTable ? <MainTable ref="mainTableChild" stubVars={this.props.stubVars.map(stubVar => stubVar.name)} 
                bannerVars={this.props.bannerVars.map(bannerVar => bannerVar.name)} options={this.props.options} meta={this.props.meta} 
                data={this.props.data} /> : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MainScreen
