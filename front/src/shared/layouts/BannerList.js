import React, { Component } from 'react'
import layoutHelpers from './helpers'
import {Row, Col, ListGroup} from 'react-bootstrap'
import styles from '../../css/custom.css'
import { DropTarget } from 'react-dnd'
import  ItemTypes  from './ItemTypes'
import MiscHelpers from './MiscHelpers'

class BannerList extends Component {
  constructor(props) {
    super(props);
}

componentDidUpdate(prevProps) {
}

render() {
  const { connectDropTarget, isOver } = this.props
  const variables = this.props.bannerVars.map((bannerVar, bannerVarIndex) => {
    return <li key={bannerVar.name} style={{listStyle: 'none', display: 'inline-block'}} className='nav-item mr-2'>
      <div>
        <span>
          <i className={MiscHelpers.variableIcon(bannerVar.type)}></i>
            {MiscHelpers.checkVarNameLength(bannerVar.name)}
          </span>
        <span className='ml-1 fal fa-trash-alt' style={{cursor: 'pointer'}} onClick={()=> {this.props.deleteDraggedVar(bannerVarIndex, 'bannerList')}}></span>
      </div>
    </li>
  })
    return connectDropTarget(
      <div className="card h-100" style={isOver ? {backgroundColor: 'lightgray'} : null}>
        <div className="card-body">
          <h6 className="card-title">Banner Variables</h6>
            <ul id="banner-list" className="variables-list p-0 w-100" style={{listStyleType: 'none',
              minHeight: '50px', textDecoration: 'none', paddingInlineStart: '0%'}}>
              {variables}
            </ul>
        </div>
      </div>)
  }
}

export default DropTarget(
  ItemTypes.VariableList,
  {
    drop(props, monitor, component) {
      return { name: 'BannerList' }
    }
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
  }),
)(BannerList)
