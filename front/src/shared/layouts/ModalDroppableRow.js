import { useDrag, useDrop } from 'react-dnd'
import EditableLabel from 'react-inline-editing';
import React, { useState, useRef } from 'react';
import  ItemTypes  from './ItemTypes'

function getStyle(backgroundColor, height) {
  return {
    height,
    backgroundColor,
  }
}

const ModalDroppableRow = (props) => {
  const [hasDropped, setHasDropped] = useState(false)
  const ref = useRef(null)
  const [{ isOver, isOverCurrent, draggedItem }, drop] = useDrop({
    accept: ItemTypes.ModalRow,
    drop(item, monitor) {
      if(!props.droppable){
        const dropped = monitor.getDropResult()
        console.log(dropped)
        const didDrop = monitor.didDrop()
        if (didDrop) {
          return
        }
        setHasDropped(true)
      }
      else{
        return { name: 'emptyRow' }
      }

    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      isOverCurrent: monitor.isOver({ shallow: true }),
      draggedItem: monitor.getItem()
    }),
  })
  var backgroundColor, height;
  if(isOverCurrent && props.droppable){
    backgroundColor = 'lightblue';
    height = '50px'
  }
  drop(ref)
  return <tr ref={ref} style={getStyle(backgroundColor, height)}>
      <th>{props.mRValue.value}</th>
      <th>{props.mREdit ? <EditableLabel text={props.mRValue.text['en-GB']}
                  labelClassName='m-0'
                  inputClassName='myInputClass'
                  inputWidth='150px'
                  inputHeight='20px'
                  inputMaxLength={50}
                  labelFontWeight='normal'
                  inputFontWeight='normal'
                  onFocusOut={(text) => {props.labelFocusOut(text, props.mRValueIdx)}}
                  />: props.mRValue.text['en-GB']}
      </th>
      <th name='iconCol' style={{ maxWidth: '1rem',
                                            width: '1em',
                                            backgroundColor: 'white',
                                            borderTop: 'hidden',
                                            borderRight: 'hidden',
                                            borderBottom: 'hidden'}}>
          <i className="far fa-minus-square" style={{cursor: 'pointer'}} onClick={() => props.deleteRow(props.mRValue.value)}></i>
      </th>
      <th style={{ maxWidth: '1rem',
                              width: '1em',
                              backgroundColor: 'white',
                              borderTop: 'hidden',
                              borderRight: 'hidden',
                              borderBottom: 'hidden'}}>
        {props.droppable && <i className="far fa-check-square" style={{fontSize:'0.9rem', cursor: 'pointer'}} onClick={() => props.setDroppable(false)}></i>}
      </th>
  </tr>
}
export default ModalDroppableRow