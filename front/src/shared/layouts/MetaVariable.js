import React, { Component } from 'react'
import styles from '../../css/custom.css'
import { DragSource } from 'react-dnd'
import  ItemTypes  from './ItemTypes'
import MiscHelpers from './MiscHelpers'

class MetaVariable extends Component {
  constructor(props) {
    super(props);

}
render() {
  const { beginDrag, endDrag, connectDragSource, isDragging } = this.props;
    return (
      <li ref={connectDragSource} style={{opacity: isDragging ? 0.4 : 1}}>
        <div className= { isDragging ? 'dragged-nav-link' : 'nav-link d-flex align-items-center justify-content-between pr-2'}>
          <span>
            <i className={MiscHelpers.variableIcon(this.props.type)}></i>
            {MiscHelpers.checkVarNameLength(this.props.name)}
          </span>
          <div style={{width: '25px', display: 'flex', justifyContent: 'center'}} onClick={()=> this.props.modalInfo(true, this.props.name, 'variables')}>
          <span className='fas fa-ellipsis-v'></span>

          </div>
        </div>
      </li>
    )
  }
}

export default DragSource(
  ItemTypes.VariableList,
  {
    beginDrag: (props) => ({ name: props.name }),
    endDrag(props, monitor) {
      const item = monitor.getItem()
      console.log(props, monitor);
      const dropResult = monitor.getDropResult()
      if (dropResult) {
        props.dragVar(item.name, props.type, dropResult.name)
      }
    },
    canDrag(props, monitor){
      console.log(props)
      return props.type === "single" | props.type === "delimited set" ? true : false;
    }
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
  }),
)(MetaVariable)
