import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavLink, Link } from 'react-router-dom'
import { Navbar, Nav, NavDropdown, Button, NavItem } from 'react-bootstrap'
import layoutHelpers from './helpers'
import dejuice_logo from '../../img/dejuice_orange_segment_sml_grn.png'
import axios from 'axios'

const FormData = require('form-data');

class LayoutNavbar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fileInputPlaceholder: "Choose file",
      excelFileInputPlaceholder: "Choose file",
      file: null,
      excelFile: null,
      meta: null,
      data: null,
      showButton: false,
      datasmoothieToken: '',
    }
    this.isRTL = document.documentElement.getAttribute('dir') === 'rtl'
    
    // This binding is necessary to make `this` work in the callback
    this.handleFile = this.handleFile.bind(this);
    this.handleExcelFile = this.handleExcelFile.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.handleExcelUpload = this.handleExcelUpload.bind(this);

    this.handleData = this.handleData.bind(this);
    this.handleDataFromExcel = this.handleDataFromExcel.bind(this);
    this.handleDataSmoothieGetData = this.handleDataSmoothieGetData.bind(this);
  }

  toggleSidenav(e) {
    e.preventDefault()
    layoutHelpers.toggleCollapsed()
  }
  
  handleFile(e){
    let file = e.target.files[0]
    this.setState({
      fileInputPlaceholder: file.name,
      file: file,
    });
  }

  handleExcelFile(e){
    let file = e.target.files[0]
    this.setState({
      excelFileInputPlaceholder: file.name,
      excelFile: file
    });
  }

  handleUpload(e){
    let file = this.state.file
    let data = new FormData()
    // data.append('file', file)
    data.append('sav_file', file)
    axios({
      // baseURL: "https://api.spectabulate.com/",
      // baseURL: "http://localhost:8080",
      baseURL: "https://mn99wezl2h.execute-api.us-east-1.amazonaws.com/production",
      url: "/sav_convert",
      method: "POST",
      headers: { 
        'content-type': 'multipart/form-data',
        'Access-Control-Allow-Origin':"*"
      },
      data: data
    }).then(res => {
      console.log(res.data);
      this.handleData(res);

    }, err => {console.log(err)});  
  }

  handleExcelUpload(e){
    let file = this.state.excelFile
    let data = new FormData()
    data.append('file', file)
    axios({
      // baseURL: "http://localhost:4000",
      baseURL: "https://mn99wezl2h.execute-api.us-east-1.amazonaws.com/production",
      url: "/upload_excel",
      method: "POST",
      headers: { 'content-type': 'multipart/form-data' },
      data: data
    }).then(res => {
      console.log(res.data)
      this.handleDataFromExcel(res.data);
    }, err => {console.log(err)});   
  }

  myChangeHandler = (event) => {
    console.log(event.target.value);
    this.setState({datasmoothieToken: event.target.value});
  }

  handleDataSmoothieGetData(event){
    // event.preventDefault();
    // console.log(this.state.datasmoothieToken);
    const token = this.state.datasmoothieToken;

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Token "+token);
    myHeaders.append("Content-type", "application/json");

    var requestOptions = {
      method: 'GET',
      mode: 'cors',// *cors, same-origin
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("https://www.datasmoothie.com/api2/datasource/", requestOptions)
      .then(response => response.text())
      .then(result => console.log(JSON.parse(result)))
      .catch(error => console.log('error', error));
  }
  
  handleData(res){
    this.setState({ 
      meta: res.data.meta,
      data: res.data.data,
      showButton: true
     });
    this.props.cbFromParent(this.state.meta, this.state.data);          
  }
  dataValue(newVarDef,dataRow){
    newVarDef.forEach((varDefinition)=>{;
        varDefinition.values.forEach((catDefinition,catDefinitionIndex)=>{
            let defDetail = catDefinition['definition'];
            if(defDetail){
              if(defDetail.includes(' and ')){
                  let defElements = defDetail.split(' and ');
                  let compoundDefMet = [];
                  defElements.forEach((compoundDef, compoundDefIndex)=>{
                      let defElementsCompound = compoundDef.split("=");
                      if(typeof dataRow[defElements[0]]!==null && dataRow[defElementsCompound[0]]===Number(defElementsCompound[1])){
                          compoundDefMet.push(true);
                      }else{
                          compoundDefMet.push(false);
                      }
                  })
                  
                  if(compoundDefMet.indexOf(false)===-1){
                      dataRow[varDefinition.name]=catDefinitionIndex+1;
                      return
                  }else{
                      if(dataRow[varDefinition.name]>0){
                          dataRow[varDefinition.name]=dataRow[varDefinition.name];
                          return
                      }else{
                          dataRow[varDefinition.name]=null;
                          return
                      }
                  }
                  
              }
              else{
                  let defElements = defDetail.split("=");
                  if(defElements[0]==="compute"){
                      dataRow[varDefinition.name]=Number(defElements[1]);
                      return
                  }else if(typeof dataRow[defElements[0]]!=='undefined' && defElements[1].indexOf(",")>-1){
                      let valueList= defElements[1].split(",");
                      if(typeof dataRow[defElements[0]]!==null){
                          if(valueList.length>0 && dataRow[defElements[0]]!==null && valueList.includes(dataRow[defElements[0]].toString())){
                              dataRow[varDefinition.name]=catDefinitionIndex+1;
                              return
                          }
                          else{
                              if(dataRow[varDefinition.name]>0){
                                  dataRow[varDefinition.name]=dataRow[varDefinition.name];
                                  return
                              }else{
                                  dataRow[varDefinition.name]=null;
                                  return
                              }
                          }
                      }
                  }else if(typeof dataRow[defElements[0]]!=='undefined' &&  defElements[1].indexOf("-")>-1){
                      let range = defElements[1].slice(1,-1);
                      let valueList= range.split("-");
                      if(typeof dataRow[defElements[0]]!==null){
                          if(valueList.length>0 && dataRow[defElements[0]]!==null && (dataRow[defElements[0]]>=Number(valueList[0]) && dataRow[defElements[0]]<=Number(valueList[1]))){
                              dataRow[varDefinition.name]=catDefinitionIndex+1;
                              return
                          }
                          else{
                              if(dataRow[varDefinition.name]>0){
                                  dataRow[varDefinition.name]=dataRow[varDefinition.name];
                                  return
                              }else{
                                  dataRow[varDefinition.name]=null;
                                  return
                              }
                          }
                      }
                  }
                  else if(typeof dataRow[defElements[0]]!=='undefined' &&  typeof dataRow[defElements[0]]!==null){
                      if(dataRow[defElements[0]]===Number(defElements[1])){
                          dataRow[varDefinition.name]=catDefinitionIndex+1;
                          return
                      }
                      else{
                          if(dataRow[varDefinition.name]>0){
                              dataRow[varDefinition.name]=dataRow[varDefinition.name];
                              return
                          }else{
                              dataRow[varDefinition.name]=null;
                              return
                          }
                      }
                      
                  }

              }
            }
        })
    })
    return dataRow;
  }
  handleDataFromExcel(res){
    let existingMeta = this.state.meta;
    let existingData = this.state.data;
    let newVarDef = res;
    newVarDef.forEach((metaVal, metaValIndex)=>{
      var newVar = {};
      newVar.name = metaVal.name;
      newVar.parent= {};
      newVar.text = {};
      newVar.type="single";
      newVar.values = [];
      newVar.text['en-GB'] = metaVal.label;
      metaVal.values.forEach((category, categoryIndex)=>{
        newVar.values.push({
          text: {'en-GB': category.label},
          value: categoryIndex+1
        })
      })
      existingMeta.columns[metaVal.name]=newVar;
      existingMeta.sets['data file'].items.push("columns@"+metaVal.name);
    })

    existingData.forEach(d => {
      d = this.dataValue(newVarDef, d);
    })

    console.log(existingData);

    this.setState({ 
      meta: existingMeta,
      data: existingData
    });
    this.props.cbFromParent(this.state.meta, this.state.data);
  }

  render() {
    let currentPath = this.props.location.pathname;
    return (
      <Navbar bg={this.props.navbarBg} expand="lg" className="layout-navbar align-items-lg-center container-p-x">

        {/* Brand */}
        <Navbar.Brand as={NavLink} to="/">
          <Link to="/" className="nav-link p-0">
            <img src={dejuice_logo} alt="dejuice_logo" style={ {position: 'relative', bottom: '2px'} } />
          </Link>
        </Navbar.Brand>
        {/* Navbar toggle */}
        <Navbar.Toggle  aria-controls="basic-navbar-nav" />

        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav>
            <Link to="/main/weight_builder" className="nav-link" style={{display: currentPath === '/main/weight_builder' ? 'none' : 'block'}}><span><i className="fal fa-weight"></i> Weight Builder</span></Link>
            <Link to="/main/table_builder" className="nav-link" style={{display: currentPath === '/main/table_builder' ? 'none' : 'block'}}><span><i className="fal fa-table"></i> Table Builder</span></Link>
            {/* <Link to="/main/spec_builder" className="nav-link" style={{display: currentPath === '/main/spec_builder' ? 'none' : 'block'}}><span><i className="fal fa-table"></i> Spec Builder</span></Link> */}
            <NavDropdown style={{display:this.state.showButton ? 'block' : 'none'}} className='mr-3' alignRight title={<span><i className="fas fa-file-excel"></i> Upload Spec file</span>} id="excel-nav-dropdown">
              <form ref='excelUploadForm' id='excelUploadForm' encType="multipart/form-data">
                <div className="custom-file m-2" style={{width: '25rem', height: '4.5rem'}}>
                  <input id="inputExcelFile" type="file" className="custom-file-input" onChange={this.handleExcelFile}/>
                  <label className="custom-file-label" htmlFor="inputExcelFile">{this.state.excelFileInputPlaceholder}</label>
                  <NavDropdown.Item className="p-0">
                    <Button className="my-2" size="sm" onClick={this.handleExcelUpload} variant="secondary">Upload!</Button>
                  </NavDropdown.Item>
                </div>
              </form>
            </NavDropdown>            
            <NavDropdown alignRight title={<span><i className="fas fa-database"></i> Data</span>} id="basic-nav-dropdown">
              <form ref='uploadForm' id='uploadForm' encType="multipart/form-data">
                <div className="custom-file m-2" style={{width: '25rem', height: '4.5rem'}}>
                  <input id="inputFile" type="file" className="custom-file-input" onChange={this.handleFile}/>
                  <label className="custom-file-label" htmlFor="inputFile">{this.state.fileInputPlaceholder}</label>
                  <NavDropdown.Item className="p-0">
                    <Button className="my-2" size="sm" onClick={this.handleUpload} variant="secondary">Upload!</Button>
                  </NavDropdown.Item>
                </div>
              </form>
            </NavDropdown>
            {/* <NavDropdown alignRight title={<span><i className="fas fa-database"></i> Datasmoothie Data</span>} id="datasmoothie-nav-dropdown">
              <form ref='datasmoothieForm' id='datasmoothieForm'>
                <div className="custom-file m-2" style={{width: '25rem', height: '4.5rem'}}>
                  <label htmlFor="datasmoothie_token">Datasmoothie Token ID</label>
                  <input type="password" className="form-control" onChange={this.myChangeHandler} id="datasmoothie_token" placeholder="Enter Datasmoothie Token" autoComplete="new-password"/>
                  <NavDropdown.Item className="p-0">
                    <Button className="my-2" size="sm"  type="button" variant="secondary" onClick={this.handleDataSmoothieGetData}>Get Datasmoothie Datasource List</Button>
                  </NavDropdown.Item>
                </div>
              </form>
            </NavDropdown> */}
          </Nav>
        </Navbar.Collapse>

      </Navbar>
    )
  }
}

LayoutNavbar.propTypes = {
  sidenavToggle: PropTypes.bool
}

LayoutNavbar.defaultProps = {
  sidenavToggle: true
}

export default connect(store => ({
  navbarBg: store.theme.navbarBg
}))(LayoutNavbar)
