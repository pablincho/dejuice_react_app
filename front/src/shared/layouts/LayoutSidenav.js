import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Sidenav from '../../vendor/libs/sidenav'
import layoutHelpers from './helpers'
import TablesList from './TablesList'
import MetaVariable from './MetaVariable'
import {Row, Col} from 'react-bootstrap'
import styles from '../../css/custom.css'
import FeatherIcon from 'feather-icons-react';
class LayoutSidenav extends Component {
  constructor(props) {
    super(props)
    
    // This binding is necessary to make `this` work in the callback
    // this.handleFile = this.handleFile.bind(this);
    // this.handleUpload = this.handleUpload.bind(this);
  }
  componentDidUpdate(prevProps) {
  }
  layoutSidenavClasses() {
    let bg = this.props.sidenavBg
    if (this.props.orientation === 'horizontal' && (bg.indexOf(' sidenav-dark') !== -1 || bg.indexOf(' sidenav-light') !== -1)) {
      bg = bg
        .replace(' sidenav-dark', '')
        .replace(' sidenav-light', '')
        .replace('-darker', '')
        .replace('-dark', '')
    }

    return `bg-${bg} ` + (
      this.props.orientation !== 'horizontal'
        ? 'layout-sidenav'
        : 'layout-sidenav-horizontal container-p-x flex-grow-0'
    )
  }
  toggleSidenav(e) {
    e.preventDefault()
    layoutHelpers.toggleCollapsed()
  }

  isMenuActive(url) {
    return this.props.location.pathname.indexOf(url) === 0
  }

  isMenuOpen(url) {
    return this.props.location.pathname.indexOf(url) === 0 && this.props.orientation !== 'horizontal'
  }
  render() {
    return (
      <Sidenav style={{width: '14.3rem'}} orientation={this.props.orientation} className={this.layoutSidenavClasses()}>
        {/* Inner */}
        <TablesList sendTables = {this.props.sendTables} addTable={this.props.addTable} deleteTable={this.props.deleteTable}
        showSavedVars={this.props.showSavedVars} showModal={this.props.showModal} currentTable={this.props.currentTable}/>
        <Row style={{width: '100%'}}>
        <Col className="pr-0">
        <h6 className="sidebar-heading d-flex justify-content-between align-items-center pl-3 mt-2 mb-1 text-muted">
          <span>Variables List</span>
          <FeatherIcon icon="plus-circle" />
        </h6>
        <div id='varsList' style={{maxHeight: '100vh', overflowY: 'scroll', overflowX: 'hidden'}}>
          <ul className="nav flex-column mb-2 sidebar">
            {this.props.sendMeta.map(variable => {
              return <MetaVariable key={variable.name} name={variable.name} type={variable.type} 
              modalInfo = {this.props.modalInfo} dragVar = {this.props.dragVar}/>
            })}
          </ul>
        </div>
        </Col>
      </Row>
      </Sidenav>
    )
  }
}

LayoutSidenav.propTypes = {
  orientation: PropTypes.oneOf(['vertical', 'horizontal'])
}

LayoutSidenav.defaultProps = {
  orientation: 'vertical'
}

export default connect(store => ({
  sidenavBg: store.theme.sidenavBg
}))(LayoutSidenav)
