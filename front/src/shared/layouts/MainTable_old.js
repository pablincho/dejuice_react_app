import React, { Component } from 'react'
import styles from '../../css/custom.css'
import axios from 'axios'
import { cloneDeep } from 'lodash';

class MainTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableResult: {}
    }
    this.multiVariableStub = this.multiVariableStub.bind(this);
}

multiVariableStub = (multiVar,meta,data) => {
  let newMultiVars = [];
  meta.columns[multiVar].values.forEach(function (multiCategory, multiCategoryIndex) {
      var newVar = {};
      newVar.text = {};
      newVar.values = [];
      newVar.name = meta.columns[multiVar].name.toUpperCase()+"_"+multiCategory.value.toString();
      newVar.text['en-GB'] = multiCategory.text['en-GB'];
      meta.columns[newVar.name] = newVar;
      meta.sets['data file'].items.push('columns@' + newVar.name);
      newVar.values.push({
          text: {'en-GB': multiCategory.text['en-GB']},
          value: multiCategory.value
      })
      data.forEach((dataRow, dataRowIndx) => {
        if(Array.isArray(dataRow[multiVar])){
          if(dataRow[multiVar][multiCategory.value - 1]===1){
            console.log(dataRow[multiVar]);
            dataRow[newVar.name] = multiCategory.value;
          }
        }
      });
      newMultiVars.push(newVar.name);
  });
  return [newMultiVars, meta, data];
}

drawTable = () =>{
  console.log(this.props)
  const stubVars = this.props.stubVars
  const bannerVars = this.props.bannerVars
  const meta = this.props.meta
  const data = this.props.data
    var tableParameters = {
      'stubvars': stubVars, 
      'bannervars': bannerVars,
      'dataMeta' : meta,
      'dataVars' : data,
      'options' : this.props.options
    };
    console.log(tableParameters)
    axios({
      baseURL: "http://localhost:4000",
      url: "/getTable",
      method: "POST",
      headers: { 'content-type': 'application/json' },
      data: JSON.stringify(tableParameters)
    }).then(res => 
      {
        console.log(res.data);
        const tableResult = res.data;

        this.setState({tableResult})
    }, err => {console.log(err)});
}
componentDidMount(prevProps){
    this.drawTable()
}

componentDidUpdate(prevProps){
  if (this.props.stubVars !== prevProps.stubVars || this.props.bannerVars !== prevProps.bannerVars 
    || this.props.meta !== prevProps.meta || this.props.data !== prevProps.data) {
      this.drawTable();
  }  
}

render() {
  const tableResult = this.state.tableResult
  console.log(tableResult)
  if(!isEmpty(tableResult)){
    var variables = this.props.stubVars.map((stubvarelement, stubvarelementIndex) => {
      return <>
        {stubvarelementIndex === 0 ? <thead key='thead'><tr key={stubvarelementIndex + '0'}><th key='pad0'></th>{bannerVars.map(bannerVar => {
          return <th key={bannerVar + '0'} className='table_header border' colSpan={varMeta.columns[bannerVar].values.length.toString()}>{varMeta.columns[bannerVar].text['en-GB']}</th>
        })}</tr>
          <tr key={stubvarelementIndex + '1'}><th key='pad1'></th>{bannerVars.map(bannerVar => {
            return <>{varMeta.columns[bannerVar].values.map((value, idx)  => <th key={idx} className="border">{value.text['en-GB']}</th>)}</>
          })}</tr>
          {options.base ? <tr key={stubvarelementIndex + '2'}><th key='uWS' className="base">Unweighted Sample</th>{bannerVars.map((bannerVar, bannerVarElementIndex) => {
            return <>{varMeta.columns[bannerVar].values.map((bannerCategoryItem, bCIIdx) => {
              var bannerUnWeightedArrays = 0;
              if(tableResult.stubVarTables[stubvarelementIndex]){
                if(tableResult.stubVarTables[stubvarelementIndex].TableDefinition[bannerVarElementIndex]){
                  bannerUnWeightedArrays = tableResult.stubVarTables[stubvarelementIndex].TableDefinition[bannerVarElementIndex].find(x => x.row_var_code === "base_row");
                }
              }
              if (bannerUnWeightedArrays){
                var unWeightedCellbases = bannerUnWeightedArrays.values.find(x => x.banner_var_code === bannerCategoryItem.value.toString());
              }
            return unWeightedCellbases ? <th key={bCIIdx + '0'} className="border">{unWeightedCellbases.counts}</th> : <th key={bCIIdx + '1'} className="border"></th>
            })}</>
          })}</tr> : null}
          {options.wBase && options.wVar !== '' ? <tr key={stubvarelementIndex + '3'}><th key='wS' className="weighted-base">Weighted Sample</th>{bannerVars.map((bannerVar, bannerVarElementIndex) => {
            return <>{varMeta.columns[bannerVar].values.map((bannerCategoryItem, bCIIdx) => {
              var bannerWeightedArrays = 0;
              if(tableResult.stubVarTables[stubvarelementIndex]){
                if(tableResult.stubVarTables[stubvarelementIndex].TableDefinition[bannerVarElementIndex]){
                  bannerWeightedArrays = tableResult.stubVarTables[stubvarelementIndex].TableDefinition[bannerVarElementIndex].find(x => x.row_var_code === "weighted_base_row");
                }
              }
              if (bannerWeightedArrays){
                var weightedCellbases = bannerWeightedArrays.values.find(x => x.banner_var_code === bannerCategoryItem.value.toString());
              }
            return weightedCellbases ? <th key={bCIIdx + '2'} className="border">{weightedCellbases.weightedcounts}</th> : <th key={bCIIdx + '3'} className="border"></th>
            })}</>
          })}</tr>: null}
          {options.percentages && !options.counts ? <tr key={stubvarelementIndex + '4'}><th key='pad2'></th>{bannerVars.map((bannerVar, bannerVarElementIndex) => {
            return <>{varMeta.columns[bannerVar].values.map((value, idx) => <th key={idx + '1'}>%</th>)}</>
          })}</tr> : null}
        </thead> : null}
        <tbody key='tbody'>
        <tr key='description' className="stub-text-row"><th key='descVar' scope="row" >{varMeta.columns[stubvarelement].text["en-GB"]}</th></tr>
          {varMeta.columns[stubvarelement].values.map((stubVarCatgetory, stubVarCatgetoryIndex) =>{
            return <>
              <tr key={stubVarCatgetoryIndex} className="category-row"><th key='stubVarCatgetoryText' >{stubVarCatgetory.text["en-GB"]}</th>{bannerVars.map((bannerVar, bannerVarElementIndex) => {
                return <>{varMeta.columns[bannerVar].values.map((bannerCategoryItem, bCIIdx) => {
                  var bannerCellArrays = 0;
                  if(tableResult.stubVarTables[stubvarelementIndex]){
                    if(tableResult.stubVarTables[stubvarelementIndex].TableDefinition[bannerVarElementIndex]){
                      var bannerCellArrays = tableResult.stubVarTables[stubvarelementIndex].TableDefinition[bannerVarElementIndex].find(x => x.row_var_code === stubVarCatgetory.value.toString());
                    }
                  }
                  if (bannerCellArrays){
                    var bannerCellItems = bannerCellArrays.values.find(x => x.banner_var_code ===bannerCategoryItem.value.toString());
                    if (bannerCellItems){
                      if(options.counts && !options.percentages){
                        return <th key={bCIIdx + 'cnp'}>{bannerCellItems.counts}</th>
                      }
                      if(options.percentages && !options.counts){
                        return <th key={bCIIdx + 'pnc'}>{bannerCellItems.percentages}</th>
                      }
                      if(options.percentages && options.counts){
                        return <th key={bCIIdx + 'p&c'}>{<>{bannerCellItems.counts.toString()}<br key={bCIIdx + 'linebr'}/><b key={bCIIdx + 'perc'}>{bannerCellItems.percentages.toString()}</b>%</>}</th>
                      }
                      else{
                        return <th key={bCIIdx + 'full'}></th>
                      }
                    }
                    else{
                      return <th key={bCIIdx + 'none'}></th>
                    }
                  }
                })}</>
              })}</tr>
              </>
            }
          )}
        </tbody>
        </>
      })}

    return <div id="table_holder" className="table-responsive mx-3">
      <table className="table-sm">
      {variables}
      </table>
    </div>

  }
}

export default MainTable
