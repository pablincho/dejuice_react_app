import React, { Component } from 'react'
import layoutHelpers from './helpers'
import {Row, Col, ListGroup} from 'react-bootstrap'
import styles from '../../css/custom.css'
import { DropTarget } from 'react-dnd'
import  ItemTypes  from './ItemTypes'
import MiscHelpers from './MiscHelpers'

class WeightingList extends Component {
  constructor(props) {
    super(props);
}

componentDidUpdate(prevProps) {
}

render() {
  console.log(this.props)
  const { connectDropTarget, isOver } = this.props
  // const { connectDropTarget, isOver } = this.props
  // const variables = this.props.stubVars.map((stubVar, stubVarIndex) => {
  //   return <li key={stubVar.name} style={{listStyle: 'none'}} className='my-1'>
  //     <div>
  //       <span>
  //         <i className={MiscHelpers.variableIcon(stubVar.type)}></i>
  //           {MiscHelpers.checkVarNameLength(stubVar.name)}
  //         </span>
  //       <span className='ml-1 fal fa-trash-alt' style={{cursor: 'pointer'}} onClick={()=> {this.props.deleteDraggedVar(stubVarIndex,'stubList')}}></span>
  //     </div>
  //   </li>
  // })
    return connectDropTarget(
      <div id="weight_card" className="card h-100" style={isOver ? {backgroundColor: 'lightgray'} : null}>
        <div className="card-body">
          <h6 className="card-title">Stub Variables</h6>
          <ul id="stub-list" className="variables-list ui-sortable h-100 p-0">
          </ul>
        </div>
      </div>)
  }
}

export default DropTarget(
  ItemTypes.VariableList,
  {
    drop(props, monitor, component) {
      return { name: 'WeightingList' }
    }
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
  }),
)(WeightingList)
