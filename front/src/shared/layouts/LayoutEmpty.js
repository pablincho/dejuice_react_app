import React, { Component } from 'react'

class LayoutEmpty extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (<>{this.props.children}</>)
  }
}

export default LayoutEmpty
