import React from 'react'
import loadable from '@loadable/component'
import pMinDelay from 'p-min-delay'
import Loader from './shared/Loader'

// Layouts
import Layout1 from './shared/layouts/Layout1'
import LayoutEmpty from './shared/layouts/LayoutEmpty' 
// Lazy load component
const lazy = (cb) => loadable(() => pMinDelay(cb(), 200), { fallback: <Loader /> })

// ---
// Default application layout

export const MainLayout = Layout1

// ---
// Document title template

export const titleTemplate = '%s - React Starter'

// ---
// Routes
//
// Note: By default all routes use { "exact": true }. To change this
// behaviour, pass "exact" option explicitly to the route object

export const defaultRoute = '/table_builder'
export const homeRoutes = [
  {
    path: '/',
    component: lazy(() => import('./components/Home')),
    layout: LayoutEmpty
  },
  {
    path: '/main',
    component: lazy(() => import('./components/TableBuilder')),
    layout: MainLayout
  },
]
export const mainRoutes = [
  {
    path: '/main/table_builder',
    component: lazy(() => import('./components/TableBuilder'))
  }, {
    path: '/main/weight_builder',
    component: lazy(() => import('./components/WeightBuilder'))
  }, 
  // {
  //   path: '/main/spec_builder',
  //   component: lazy(() => import('./components/SpecBuilder'))
  // }
]
